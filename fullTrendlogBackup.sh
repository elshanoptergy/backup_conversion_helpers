#!/bin/bash

#This script handles createing a full trendlog back, all trendlogs and all data. Creates backup files into multiple parts, 500 trendlogs per part
# For a manual backups pass the description, creator and filename so that fields are popul;ated correctly
# For auto backups no parameters need to be passed they will all be created automatically

#If auto backup description will be empty, otherise indicates that this is a manual backup
DESC=$1
CREATOR=$2
FILENAME=$3

#Set auto creator is none was passed
if [ -z "${CREATOR}" ]; then
  CREATOR="System Auto Backup"
fi

#Determine which directory backup will be made
AUTODIR="/usr/local/Optergy/Backup"
if [ -z "${DESC}" ]; then
  AUTODIR="$AUTODIR/Auto"
fi
TEMPDIR="/usr/local/Optergy/Backup/Auto/Trendlog"
TRENDLOGDIR="/usr/local/Optergy/Backup/Auto/Trendlog/Trendlogs"

#Create backup name
BACKUPNAME="Trendlog"
if [ -z "${DESC}" ]; then
  BACKUPNAME="Auto-$BACKUPNAME"
fi

#Set auto descrption
if [ -z "${DESC}" ]; then
  DESC="System auto generated backup file"
fi

#Get current date and time
NOW=$(date +"%m-%d-%Y %r")
echo $NOW

DATE=$(date +"%Y-%m-%d")
TIME=$(date +"%r")

#Set file name.
if [ -z "${FILENAME}" ]; then
  FILENAME="$BACKUPNAME-$DATE"
fi

#Get current optergy version
VERSION=`sudo dpkg -s optergy | grep Version | awk '{print $2}'`

#Keep track of backup part
INDEX=1
PART=1

#Make temp directory
sudo mkdir $TEMPDIR
sudo chown optergy. $TEMPDIR

#Make Trendlogs directory
sudo mkdir $TRENDLOGDIR
sudo chown optergy. $TRENDLOGDIR

#Create description.txt
echo $DESC > $TEMPDIR/description.txt

# Create zip
ZIP="$AUTODIR/$FILENAME-part$PART.backup"
zip -j $ZIP $TEMPDIR/description.txt

# Create a summary
echo "Backup Type: Trendlog" > $TEMPDIR/summaryOrig.txt
echo "Date Created: $DATE $TIME" >> $TEMPDIR/summaryOrig.txt
echo "Created By: $CREATOR" >> $TEMPDIR/summaryOrig.txt
echo "Software Version: $VERSION" >> $TEMPDIR/summaryOrig.txt
echo "Time Period: All Data" >> $TEMPDIR/summaryOrig.txt
echo "Trendlogs:" >> $TEMPDIR/summaryOrig.txt
cp $TEMPDIR/summaryOrig.txt $TEMPDIR/summary.txt

echo "Getting all trendlogs"
rows=`echo "SELECT tablename FROM pg_tables WHERE tablename LIKE 'tl_%_%';"  | psql trendlogs -U optergy -t`
for tablename in $rows
do
	table_string=`echo $tablename | sed 's/\n//g'`
	echo " $table_string" >> $TEMPDIR/summary.txt
	#echo "Backing up $table_string"
  pg_dump --compress=1 --file=$TRENDLOGDIR/$table_string.dbf --format=c --table=$table_string trendlogs
  # TODO maybe check this before continuing
  res=$?

  # Add backup file to zip
  CURRENTPATH=`pwd`
  cd $TEMPDIR
   zip -u -r $ZIP Trendlogs
  rm $TRENDLOGDIR/$table_string.dbf

  cd $CURRENTPATH

  if (( $INDEX % 500 == 0 ))
  then
      # Add summary to zip file
      zip -u -j $ZIP $TEMPDIR/summary.txt

      #Create new zip
      ((PART++))
      ZIP="$AUTODIR/$FILENAME-part$PART.backup"
      zip -j $ZIP $TEMPDIR/description.txt

      #Create a summary
      rm $TEMPDIR/summary.txt
      cp $TEMPDIR/summaryOrig.txt $TEMPDIR/summary.txt
  fi

   ((INDEX++))
done

# May need to add the final summary
if (( $INDEX % 500 != 0 ))
  then
      # Add summary to zip file
      zip -u -j $ZIP $TEMPDIR/summary.txt
fi

sudo gpg --passphrase test --no-tty --output $ZIP.'gpg' --symmetric $ZIP

#Remove Temp Directory
rm -r $TEMPDIR

echo "Finished"

NOW=$(date +"%m-%d-%Y %r")
echo $NOW

exit 0
