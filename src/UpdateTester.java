
import com.optergy.web.backupAndRestore.SystemRestoreThread;

import java.io.File;
import java.util.Scanner;


public class UpdateTester {
    public static void main(String[] args){
         if(args.length>0) { //command
             if (args[1] != "") { //filename
                 String backup = args[1];
                 switch (args[0].toLowerCase()) {
                     case "job":
                         System.out.println("Job backup started with " + backup+".backup");
                         OptergyBackupManipulation job = new OptergyBackupManipulation();
                         job.BackupJob(backup+".backup");
                         System.out.println("Job  backup End");
                         break;
                     case "trend":
                         System.out.println("Trendlog backup started with " + backup);
                         OptergyBackupManipulation trend = new OptergyBackupManipulation();
                         trend.BackupTrend(backup);
                         System.out.println("Trend backup End");
                         break;
                     case "energy":
                         System.out.println("Energy backup started with " + backup+".backup");
                         OptergyBackupManipulation energy = new OptergyBackupManipulation();
                         energy.BackupEnergy(backup+".backup");
                         System.out.println("Energy backup End");
                         break;
                     case "restore":
                         System.out.println("Restoring backup :" + backup);
                         OptergyBackupManipulation restoreFile = new OptergyBackupManipulation();
                         restoreFile.RestoreBackup(backup);
                         break;
                     case "res":
                         OptergyBackupManipulation b = new OptergyBackupManipulation();
                         File decrypted = b.decryptFile(new File(backup));
                         SystemRestoreThread srt = new SystemRestoreThread(null,null, null, decrypted);
                         srt.start();
                         break;
                     case "version":
                         OptergyBackupManipulation version = new OptergyBackupManipulation();
                         version.getBackupVersion(backup);
                         break;
                     case "encrypt":
                         OptergyBackupManipulation enc = new OptergyBackupManipulation();
                         enc.encryptFile(new File(backup));
                         break;
                     default:
                         System.out.println("Invalid arguments for Backup Manipulation");
                 }
             } else {
                 System.out.println("Invalid Backup File Name");
             }

         }
       // File updateFile = new File(args[0]);

        //if restore
        //res.RestoreBackup();
        //if backup
        //check file need to validate or not
        //File decrypted = Util.decrypt(updateFile);
       // System.out.println("test");
    }
    public void checkVersion(){
        try
        {
            int compare = Util.compareVersion("3.0.2", "3.0.3");
           if(compare == 1){
               System.out.println("TRUE");
           }else{
               System.out.println("FALSE");
           }
        }
        catch (NumberFormatException e)
        {
            System.out.println("Version missmatch!");
        }
    }
    public void decryptFile(String filename)
    {
      //  SocketLogger sl = getContext().createSocketLogger();
        File dirFile = new File(filename);

        if (filename.endsWith(".upd"))
        {
            if(filename.contains("proton"))
            {
               //dirFile.delete();
               System.out.println("Proton");
            }
            if(filename.contains("optergy"))
            {
                //dirFile.delete();
                System.out.println("OE");
            }

            File tmpFile = new File("/usr/local/Optergy/tmp.txt");
            String signedPath = dirFile.getAbsolutePath().replace(".upd", ".deb.gpg");
            File debInstallFile = new File(signedPath.replace(".deb.gpg", ".deb"));
            File signedFile = new File(signedPath);
            try
            {
                tmpFile.delete();
                tmpFile.createNewFile();
                tmpFile.setWritable(true);
                tmpFile.setReadable(true);

                int gpgVersion = Integer.parseInt(Util.executeUnixCommand("sudo gpg --version | grep '(GnuPG)' | sed 's/.*)//' | sed 's/ //g'", "obtaining gpg version").split("\\.")[0]);

                System.out.println("Decrypting step 1" + "\n signedPath " + signedPath +"\n decrypting " + dirFile.getAbsolutePath());
               ;
                if(gpgVersion < 2) {
                    System.out.println("version less than 2");
                    Util.executeUnixCommandIgnoreError("sudo cat /root/passphrase | sudo gpg --no-tty --passphrase-fd 0 --output " + signedPath + " --decrypt " + dirFile.getAbsolutePath(),
                            "decrypting");
                } else {
                    Util.executeUnixCommandIgnoreError("sudo cat /root/passphrase | sudo gpg --no-tty --passphrase-fd 0 --batch --yes --pinentry-mode=loopback --output " + signedPath + " --decrypt " + dirFile.getAbsolutePath(),
                            "decrypting");
                }

                //confirm deb.gpg file is there
                if(!signedFile.exists()) {
                    System.out.println(signedFile);
                    System.out.println("File not exists to decrypt " + dirFile.getName());
                }else{
                    System.out.println("Sign files exist : " + signedFile.getAbsolutePath());
                }

                System.out.println("copy verification content");
                if(gpgVersion < 2) {
                    Util.executeUnixCommand("sudo gpg --verify " + signedPath + "> /usr/local/Optergy/tmp.txt 2>&1", "verifying signature");
                } else {
                    Util.executeUnixCommand("sudo gpg --no-tty --batch --yes --pinentry-mode=loopback --verify " + signedPath + "> /usr/local/Optergy/tmp.txt 2>&1", "verifying signature");
                }


                //confirm signature by regex checking for optergy@optergy.com, because if Proton/OE/864 gets cracked, signatures by support@optergy.com is possible
                boolean goodSignature = false;
                try {
                    Scanner scanner = new Scanner(tmpFile);

                    while (scanner.hasNextLine()) {
                        String line = scanner.nextLine();
                        if (line.contains("<optergy@optergy.com>")) {
                            goodSignature = true;
                        }
                    }

                    scanner.close();
                }catch(Exception e) {
                    //do nothing
                }

                if(!goodSignature) {
                    System.out.println("No good signature");
                }else{
                    System.out.println("Good signature");
                }

                System.out.println("Again going to decryot trying to make this file "+ debInstallFile.getAbsolutePath() + " from " + signedPath);
                //convert signed file to deb package
                //Util.executeUnixCommandIgnoreError("sudo gpg --no-tty --output " + debInstallFile.getAbsolutePath() + " --decrypt " + signedPath , "changing signed file to deb", socketLogger );
                Util.executeUnixCommandIgnoreError("sudo bash /usr/local/Optergy/bin/scripts/verifySig.sh " + debInstallFile.getAbsolutePath() + " " + signedPath,
                        "changing signed file to deb");

                if(!debInstallFile.exists()) {
                 System.out.println("Failed to decrypt " + dirFile.getName());
                    throw new Exception();
                }
                System.out.println("DONE");
            }
            catch (Exception e)
            {
                System.out.println("Invalid update file uploaded (" + dirFile.getAbsolutePath() + "):");
                System.out.println(e.toString());
                System.out.println("Invalid update file. Most commonly corrupted during download/upload");
            }
            finally
            {

            }
        }

        System.out.println(filename);
    }
}
