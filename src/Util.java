
import com.optergy.lib.enums.logging.LogFile;
import com.optergy.lib.exceptions.ErrorCodeException;
import com.optergy.lib.exceptions.PlatformException;
import com.optergy.lib.logging.LogUtils;
import com.optergy.lib.logging.SocketLogger;
import com.sun.deploy.config.Platform;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;



// TODO [Julian] need to revisit this file
/**
 * Convenience methods for various common tasks
 * 
 * @author Julian
 */
public class Util
{
	private static final HashMap<String, String>	packageVersions			= new HashMap<String, String>();


	public static final String						PROTON_PACKAGE_NAME		= "proton";
	public static final String						OPTERGY_PACKAGE_NAME	= "optergy";
	public static final String 						TOMCAT_PACKAGE_NAME 	= "tomcat";
	public static final String 						JAVA_PACKAGE_NAME 		= "optergy-java";
	public static final String						SPACE					= " ";
	public static final String						THREE_SPACES			= "   ";
	public static final String						NA						= " NA ";
	public static final String						COMMA					= ",";
	public static final String						EMPTY_STRING			= "";
	public static final String						METER_DISPLAY			= "meterDisplay";

	// locale STRINGS
	public static final String						AUSTRALIA_LOCALE		= "en_AU.UTF-8 UTF-8";
	public static final String						CHINA_LOCALE			= "zh_CN.UTF-8 UTF-8";
	public static final String						SOUTH_AFRICA			= "af.UTF-8 UTF-8";
	public static final String						ENGLAND_LOCALE			= "en_GB.UTF-8 UTF-8";
	public static final String						FRANCE_LOCALE			= "fr_FR.UTF-8 UTF-8";
	public static final String						GERMANY_LOCALE			= "de_DE.UTF-8 UTF-8";
	public static final String						INDONESIA_LOCALE		= "id_ID.UTF-8 UTF-8";
	public static final String						ITALY_LOCALE			= "it_IT.UTF-8 UTF-8";
	public static final String						JAPAN_LOCALE			= "ja_JP.UTF-8 UTF-8";
	public static final String						MALAYSIA_LOCALE			= "ms_MY.UTF-8 UTF-8";
	public static final String						SPAIN_LOCALE			= "es_ES.UTF-8 UTF-8";
	public static final String						USA_LOCALE				= "en_US.UTF-8 UTF-8"; 
	public static final String						SINGAPORE_LOCALE		= "en_SG.UTF-8 UTF-8";
	public static final String						ROMANIA_LOCALE			= "ro_RO.UTF-8 UTF-8";
	public static final String						GREECE_LOCALE			= "gl_GR.UTF-8 UTF-8";
	public static final String						CANADA_LOCALE			= "en_CA.UTF-8 UTF-8";
	public static final String						MEXICO_LOCALE			= "es_MX.UTF-8 UTF-8";
	public static final String						BRAZIL_LOCALE			= "pt_BR.UTF-8 UTF-8";
	public static final String 						ALGERIA_LOCALE 			= "ar_DZ.UTF-8 UTF-8";
	public static final String 						EGYPT_LOCALE 			= "ar_EG.UTF-8 UTF-8";
	public static final String 						INDIA_LOCALE 			= "en_IN.UTF-8 UTF-8";
	public static final String 						IRAQ_LOCALE 			= "ar_IQ.UTF-8 UTF-8";
	public static final String 						KOREA_LOCALE 			= "ko_KR.UTF-8 UTF-8";
	public static final String 						PHILIPPINES_LOCALE 		= "en_PH.UTF-8 UTF-8";
	public static final String 						PORTUGAL_LOCALE 		= "pt_PT.UTF-8 UTF-8";
	public static final String 						SAUDI_LOCALE 			= "ar_SA.UTF-8 UTF-8";
	public static final String 						SUDAN_LOCALE			= "ar_SD.UTF-8 UTF-8";
	public static final String 						THAILAND_LOCALE 		= "th_TH.UTF-8 UTF-8";
	public static final String 						TURKEY_LOCALE 			= "tr_TR.UTF-8 UTF-8";
	public static final String 						VIETNAM_LOCALE 			= "vi_VN.UTF-8 UTF-8";
	public static final String 						UAE_LOCALE 				= "ar_AE.UTF-8 UTF-8";
	public static final String 						PAKISTAN_LOCALE 		= "en_PK.UTF-8 UTF-8";
	/**
	 * Takes a mac address and converts it into a string which can be used to write it to the database
	 * 
	 * @param macAddress The MAC address to be processed
	 * @return The MAC address separated by commas, and surrounded by curly braces
	 */
	public static String byteArrayToString(byte[] macAddress)
	{
		StringBuilder buf = new StringBuilder("{");
		for (int i = 0; i < macAddress.length; i++)
		{
			int x = (int) macAddress[i] & 0xff;
			buf.append(x);
			if ((i + 1) < macAddress.length)
				buf.append(",");
		}
		buf.append("}");
		return buf.toString();
	}

	/**
	 * Takes an mac address as an Integer array, and converts it into a byte array
	 * 
	 * @param localMac The MAC address to be converted
	 * @return The original address, as a byte array
	 */
	public static byte[] intToByteArray(Integer[] localMac)
	{
		byte[] lmac = new byte[localMac.length];
		for (int i = 0; i < localMac.length; i++)
		{
			lmac[i] = (byte) (localMac[i] & 0xff);

		}
		return lmac;
	}

	/**
	 * <p>
	 * Takes a {@link String} that represents a name of something (in lowerCamelCase or UpperCamelCase) and builds a new
	 * {@link String} with the same text, inserting spaces before each uppercase character and ensuring that each word
	 * begins with a capital letter.
	 * </p>
	 * 
	 * @param camelCased The original {@link String} in lowerCamelCase or UpperCamelCase format.
	 * @return A {@link String} with the camelCase "humps" expanded into individual words.
	 */
	public static File decrypt(File f) {
		SocketLogger socketLogger = new SocketLogger(LogFile.WEBSERVER, Level.INFO, LogUtils.getLoggingHost());
		try
		{
			com.optergy.lib.util.Util.executeUnixCommandIgnoreError("sudo gpg --passphrase test --decrypt --no-tty --output " + "\"" + f.getAbsolutePath().split("\\.")[0] + ".decrypted\" "
					+ "\"" + f.getAbsolutePath() + "\"", "encrypting file", null);
			File decrypted = new File(f.getAbsolutePath().split("\\.")[0] + ".decrypted");
			return decrypted;
		}
		catch (IOException | InterruptedException | PlatformException | ErrorCodeException e)
		{
			socketLogger.logException(e);
			return null;
		}
		finally
		{
			socketLogger.finished();
		}
	}


	public static String camelCaseStringToMultiWordString(String camelCased)
	{
		final StringBuilder builder = new StringBuilder();
		for (int i = 0; i < camelCased.length(); ++i)
		{
			char charAt = camelCased.charAt(i);
			if (i == 0) // Takes care of situations where the first character is not uppercase.
				builder.append(Character.toUpperCase(charAt));
			else
			{
				if (Character.isUpperCase(charAt) && !Character.isUpperCase(camelCased.charAt(i - 1)))
					builder.append(' ');
				builder.append(charAt);
			}
		}
		return builder.toString();
	}

	/**
	 * Capitalizes the first letter of every word in a sentence
	 * 
	 * @param str
	 * @return
	 */
	public static String camelCaseSentence(String str)
	{
		if (str == null || str.isEmpty())
			return str;

		str = str.toLowerCase();
		String[] words = str.split(" ");

		String newSentence = "";

		for (String word : words)
		{
			if (!newSentence.isEmpty())
			{
				newSentence += " ";
			}
			newSentence += capitaliseStartOfString(word);
		}
		return newSentence;
	}

	public static String capitaliseStartOfString(String str)
	{
		if (str == null || str.isEmpty())
			return str;

		if (str.length() == 1)
			return String.valueOf(Character.toUpperCase(str.charAt(0)));

		return Character.toUpperCase(str.charAt(0)) + str.substring(1);
	}

	// TODO: merge with Kelly's implementation
	public static String formatTrailingText(String text, int cutOffLength)
	{
		if (text == null)
			return null;

		if (text.length() > cutOffLength)
		{
			return text.substring(0, cutOffLength) + " ....";
		}

		return text;
	}

	/**
	 * <p>
	 * Takes a {@link String} that will be used for database interaction (or otherwise needs to be escaped) and returns
	 * a version with quotes and slashes properly escaped to prevent the characters from being interpreted literally.
	 * </p>
	 * 
	 * <p>
	 * Examples:
	 * </p>
	 * 
	 * <ul>
	 * <li>Don't delete C:\Windows <strong> --&gt; </strong> Don\'t delete C:\\Windows</li>
	 * <li>I''ve used too many apostrophes. <strong> --&gt; </strong> I\'\'ve used too many apostrophes.</li>
	 * <li>||\\--//|| <strong> --&gt; </strong> ||\\\\--//||</li>
	 * </ul>
	 * 
	 * @param str The string to be escaped
	 * @return The original string with ' characters escaped to \' and \ characters escaped to \\.
	 * 
	 */
	public static String escapeString(String str)
	{
		if (str == null)
			return str;
		return str.replace("'", "''");
	}

	/**
	 * Checks if there is any remainder
	 * 
	 * @param was
	 * @param dividedNumber
	 * @param dividedby
	 * @return
	 */
	public static double checkDividedNumber(double was, double dividedNumber, int dividedby)
	{
		double reConstructed = dividedNumber * dividedby;
		if (reConstructed != was)
			return was - reConstructed;
		return 0;
	}

	/**
	 * Generates a URL that will automatically log in a user This URL will be stored in the database for future lookup
	 * 
	 * @param url the url to load
	 * @param the user to be automatically logged in
	 * @return a url with the token attached
	 */


	/**
	 * Generates an MD5 hash based on the input String
	 * 
	 * @param input The string to be hashed
	 * @return An MD5 hash string
	 */
	public static String makeMD5Hash(String input)
	{
		MessageDigest md;
		String hexStr = "";
		try
		{
			md = MessageDigest.getInstance("MD5");
			md.reset();
			byte[] buffer = input.getBytes();
			md.update(buffer);
			byte[] digest = md.digest();

			for (int i = 0; i < digest.length; i++)
			{
				hexStr += Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1);
			}
		}
		catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		}
		return hexStr;
	}

	/**
	 * Generates an SHA1 hash based on the input String
	 * 
	 * @param input The string to be hashed
	 * @return An SHA1 hash string
	 */
	public static String makeSHA1Hash(String input)
	{
		MessageDigest md;
		String hexStr = "";
		try
		{
			md = MessageDigest.getInstance("SHA1");
			md.reset();
			byte[] buffer = input.getBytes();
			md.update(buffer);
			byte[] digest = md.digest();

			for (int i = 0; i < digest.length; i++)
			{
				hexStr += Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1);
			}
		}
		catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		}
		return hexStr;
	}

	/**
	 * Generates random secure token to be used on app session validation
	 * 
	 * @return 130 bits from a cryptography secure random string
	 */
	public static String createToken() 
	{
        
		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        
        while (salt.length() < 30) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();

        return saltStr;

    }	
	
	/**
	 * Truncates a number to given number of decimal places
	 * 
	 * @param number raw number to truncate
	 * @param decimlaPlaces number for decimal places
	 * @return
	 */
	public static double truncate(double number, int decimalPlaces)
	{
		String num = String.format("%." + decimalPlaces + "g%n", number);
		return Double.parseDouble(num);
	}

	/**
	 * Escapes %, #, <, >, /, & and spaces with hex url encoding
	 * 
	 * @param str
	 * @return
	 */
	public static String escapeUrlString(String str)
	{
		if (str == null)
			return str;
		return str.replace("%", "%25").replace("#", "%23").replace(" ", "%20").replace("<", "%3C").replace(">", "%3E").replace("/", "%2F").replace("&", "%26");
	}

	/**
	 * Unescapes/replaces escaped %, #, <, >, /, & and spaces with the actual characters
	 * 
	 * @param str
	 * @return
	 */
	public static String unescapeUrlString(String str)
	{
		if (str == null)
			return str;
		return str.replace("%25", "%").replace("%23", "#").replace("%20", " ").replace("%3C", "<").replace("%3E", ">").replace("%2F", "/").replace("%2f", "/")
			.replace("%3A", ":").replace("%26", "&");
	}

	public static String escapeHTMLString(String str)
	{
		if (str == null)
			return str;
		return str.replace("<", "&lt;").replace(">", "&gt;").replace("'", "\'");
	}

	/**
	 * Takes a <code>String</code> that is to be written to the database, escapes all the quotes, and then surrounds the
	 * entire <code>String</code> with single quotes
	 * 
	 * @param str The <code>String</code> to be escaped and quoted
	 * @return The original <code>String</code> surrounded by a single quote character ('), with all single quotes
	 * escaped (\\).
	 */
	public static String escapeAndQuote(String str)
	{
		return "'" + escapeString(str) + "'";
	}

	/**
	 * Converts blank or <code>null</code> Strings into "null"
	 * 
	 * @param str The String to be processed
	 * @return "null" if str is <code>null</code> or a blank string, str otherwise
	 */
	public static String nullStrIfBlank(String str)
	{
		if (str == null || str.equals(""))
			return "null";
		return str;
	}

	/**
	 * Returns true if string is null or empty
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isBlank(String str)
	{
		return str == null || str.isEmpty();
	}

	/**
	 * Returns true if string is not null or empty
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isPopulated(String str)
	{
		return !isBlank(str);
	}

	/**
	 * Returns <code>null</code> if the input object is null, or obj.toString() if it is not null
	 * 
	 * @param obj An object to test if it is <code>null</code>
	 * @return A String
	 */
	public static String nullStrIfNull(Object obj)
	{
		if (obj == null)
			return "null";
		return obj.toString();
	}

	/**
	 * Returns a blank {@link String} if the input string is <code>null</code>
	 * 
	 * @param s The String to be checked
	 * @return A blank String "" if s is <code>null</code>, s otherwise
	 */
	public static String blankIfNull(String s)
	{
		if (s == null)
			return "";
		return s;
	}

	/**
	 * pads dates with a zero if need be so they are 2 digits
	 * 
	 * @return
	 */
	public static String padZero(int x)
	{
		if (x > 9)
			return "" + x;

		return "0" + x;
	}

	/**
	 * Pads a string with spaces to ensure it is at least <code>n</code> characters wide
	 * 
	 * @param s The string to be padded
	 * @param n The width of the returned padded string
	 * @return A padded string
	 */
	public static String padRight(String s, int n)
	{
		return String.format("%1$-" + n + "s", s);
	}

	/**
	 * Returns the date the portal was created
	 * 
	 * @return a {@link OptergyCalendar} containing the creation date if found, otherwise returns <code>null</code>
	 */
	public static int compareVersion(String str1, String str2) throws NumberFormatException
	{
		if(str1.contains("rc") || str1.contains("beta") || str2.contains("rc") || str2.contains("beta"))
			return 0;

		if(str1.contains("-"))
		{
			if(str1.contains("NIGHTLY"))
			{
				return 0;
			}
			else
			{
				str1 = str1.split("-")[0];
			}
		}

		if(str2.contains("-"))
		{
			if(str2.contains("NIGHTLY"))
			{
				return 0;
			}
			else
			{
				str2 = str2.split("-")[0];
			}
		}

		String endCharacter1 = str1.substring(str1.length() - 1).toLowerCase();
		String endCharacter2 = str2.substring(str2.length() - 1).toLowerCase();

		//This is to test if the last character of the version is NOT a number. If it is not a number then we add the ascii value
		//of the character to the version number. So 2.3.1c will equal 2.3.1099 (099 being the ascii value of 'c'). Else we add 000 to the
		//version number. This is so that a version number like 2.3.1c is greater than 2.3.1 or even 2.3.1b
		if(!(endCharacter1.matches("-?\\d+(\\.\\d+)?")))
		{
			char c = endCharacter1.charAt(0);
			int charAsciiNumber = (int) c;
			String asciiNumberString = Integer.toString(charAsciiNumber);
			if(endCharacter1.equals("a") || endCharacter1.equals("b") || endCharacter1.equals("c"))
			{
				str1 = str1.substring(0, str1.length() - 1) + "0" + asciiNumberString;
			}
			else
			{
				str1 = str1.substring(0, str1.length() - 1) + asciiNumberString;
			}
		}
		else
		{
			str1 = str1 + "000";
		}

		if(!(endCharacter2.matches("-?\\d+(\\.\\d+)?")))
		{
			char c = endCharacter2.charAt(0);
			int charAsciiNumber = (int) c;
			String asciiNumberString = Integer.toString(charAsciiNumber);
			if(endCharacter2.equals("a") || endCharacter2.equals("b") || endCharacter2.equals("c"))
			{
				str2 = str2.substring(0, str2.length() - 1) + "0" + asciiNumberString;
			}
			else
			{
				str2 = str2.substring(0, str2.length() - 1) + asciiNumberString;
			}
		}
		else
		{
			str2 = str2 + "000";
		}

		String[] vals1 = str1.replace("-", ".").split("\\.");
		String[] vals2 = str2.replace("-", ".").split("\\.");
		int i = 0;
		while (i < vals1.length && i < vals2.length && vals1[i].equals(vals2[i]))
		{
			i++;
		}

		if (i < vals1.length && i < vals2.length)
		{
			int diff = new Integer(vals1[i]).compareTo(new Integer(vals2[i]));
			return diff < 0 ? -1 : diff == 0 ? 0 : 1;
		}

		return vals1.length < vals2.length ? -1 : vals1.length == vals2.length ? 0 : 1;
	}
	/**
	 * Executes a unix command and waits. Also does logging - logs to a SocketLogger if one provided otherwise to
	 * sysout.
	 * 
	 * executeUnixCommand read using char[] and this using line by line.
	 * 
	 * @return Command response as a string or empty string if no response received
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws PlatformException
	 * @throws ErrorCodeException
	 */
	@SuppressWarnings("unused")

	/**
	 * Executes a unix command and waits. Also does logging - logs to a SocketLogger if one provided otherwise to
	 * sysout.
	 * 
	 * @return Command response as a string or empty string if no response received
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws PlatformException
	 * @throws ErrorCodeException
	 */
	public static String executeUnixCommand(String command, String action) throws InterruptedException, IOException
	{


		Runtime rtime = Runtime.getRuntime();
		String str = "";
		try
		{
			Process child = rtime.exec("/bin/bash");
			BufferedWriter outCommand = new BufferedWriter(new OutputStreamWriter(child.getOutputStream()));

			outCommand.write(command);
			outCommand.flush();
			outCommand.close();

			// Read input stream
			char[] c = new char[1024];
			StringBuffer sb = new StringBuffer();
			BufferedReader br = new BufferedReader(new InputStreamReader(child.getInputStream()));
			while (br.read(c) != -1)
			{
				sb.append(String.valueOf(c));
			}
			// replace char 0 with nothing - as firefox displays them as symbols
			str = sb.toString().replace(String.valueOf((char) 0), "");

			String strError = "";
			StringBuffer errorBuffer = new StringBuffer();
			// Log error stream
			BufferedReader brE = new BufferedReader(new InputStreamReader(child.getErrorStream()));
			while ((strError = brE.readLine()) != null)
			{
				errorBuffer.append(strError);

			}
			try
			{
				child.waitFor();
			}
			catch (InterruptedException e)
			{

					e.printStackTrace();

				throw e;
			}

			// Bad error code
			if (child.exitValue() != 0)
			{

					System.out.println("Error code for " + action + ": " + child.exitValue() + " " + errorBuffer.toString());


			}
			br.close();
		}
		catch (IOException e)
		{
				e.printStackTrace();

			throw e;
		}
		catch (Exception e)
		{
				e.printStackTrace();
			throw e;
		}
		return str;
	}


	/**
	 * Executes a unix command and waits. Also does logging - logs to a SocketLogger if one provided otherwise to
	 * sysout.
	 *
	 * @return Command response as a string or empty string if no response received
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws PlatformException
	 * @throws ErrorCodeException
	 */
	public static String executeUnixCommandIgnoreError(String command, String action) throws InterruptedException, IOException

	{
		// throw an exception if on a windows platform

		Runtime rtime = Runtime.getRuntime();
		String str = "";
		String strError = "";
		try
		{
			Process child = rtime.exec("/bin/bash");
			BufferedWriter outCommand = new BufferedWriter(new OutputStreamWriter(child.getOutputStream()));

			outCommand.write(command);
			outCommand.flush();
			outCommand.close();

			// Read input stream
			char[] c = new char[1024];
			StringBuffer sb = new StringBuffer();
			BufferedReader br = new BufferedReader(new InputStreamReader(child.getInputStream()));
			while (br.read(c) != -1)
			{
				sb.append(String.valueOf(c));
			}
			// replace char 0 with nothing - as firefox displays them as symbols
			str = sb.toString().replace(String.valueOf((char) 0), "");

			strError = "";
			StringBuffer errorBuffer = new StringBuffer();
			// Log error stream
			BufferedReader brE = new BufferedReader(new InputStreamReader(child.getErrorStream()));
			while ((strError = brE.readLine()) != null)
			{
				errorBuffer.append(strError);

			}
			try
			{
				child.waitFor();
			}
			catch (InterruptedException e)
			{

					e.printStackTrace();

				throw e;
			}

			br.close();
		}
		catch (IOException e)
		{

				e.printStackTrace();

			throw e;
		}
		catch (Exception e)
		{

				e.printStackTrace();

		}
		return str;
	}

}