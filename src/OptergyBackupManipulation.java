import com.optergy.lib.bean.bacnet.Trendlog;
import com.optergy.lib.bean.metering.Energylog;
import com.optergy.lib.bean.userActivity.UserActivity;
import com.optergy.lib.comparators.NaturalOrderComparator;
import com.optergy.lib.database.DatabaseControl;
import com.optergy.lib.database.UnsuitableClassException;
import com.optergy.lib.enums.UserActivityAction;
import com.optergy.lib.enums.UserActivityResult;
import com.optergy.lib.enums.logging.LogFile;
import com.optergy.lib.enums.system.Databases;
import com.optergy.lib.enums.system.SystemMessageCategory;
import com.optergy.lib.enums.systemBackup.BackupTypes;
import com.optergy.lib.enums.systemBackup.JobBackupOptions;
import com.optergy.lib.exceptions.ErrorCodeException;
import com.optergy.lib.exceptions.PlatformException;
import com.optergy.lib.licence.LicenceInvalidException;
import com.optergy.lib.licence.LicenceManager;
import com.optergy.lib.licence.LicenceValidationException;
import com.optergy.lib.localisation.LocalisationLookup;
import com.optergy.lib.logging.LogUtils;
import com.optergy.lib.logging.SocketLogger;
import com.optergy.lib.system.Paths;
import com.optergy.lib.systemBackup.SystemBackupThread;
import com.optergy.lib.util.Util;
import com.optergy.web.action.backupAndRestore.ajax.SystemBackupAndRestoreManipulation;
import com.optergy.web.database.WebDatabaseControl;
import com.optergy.web.resolution.JSONBuilder;
import com.optergy.web.resolution.JSONConverter;
import com.optergy.web.resolution.JSONResolution;
import com.optergy.web.util.SystemMessageEventUtil;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;


/**
 * Thread responsible for restore a system backup. <br>
 * Uses Servlet Context attribute to keep the web interface updated and notify the system when the restore has been
 * completed.
 *
 * @author Kelly
 */
public class OptergyBackupManipulation {
    private final String ERROR_RESTORING_AURORA_DB = ", error restoring Optergy Software database";
    /**
     * Username of the user that is restoring the backup
     */
    private List<String> energylogs;
    private String username;
    private File f;
    private String msgText;
    private Level logLevel;
    private boolean shouldValidateLicence = false;
    private List<JobBackupOptions> jobOptions = new ArrayList<JobBackupOptions>();
    ZipOutputStream zipStream = null;
    DateFormat dateFormat = LocalisationLookup.getInstance().getShortDateShortTimeFormat();
    String version = null;
    private String description = "Optergy converted backup file";

    public OptergyBackupManipulation(File f) {
        this.f = f;
        this.jobOptions.addAll(JobBackupOptions.getAll());
    }

    public OptergyBackupManipulation() {
        if (Util.isProton()) {
            version = Util.getVersion(Util.PROTON_PACKAGE_NAME);
        } else {
            version = Util.getVersion(Util.OPTERGY_PACKAGE_NAME);
        }
        this.jobOptions.addAll(JobBackupOptions.getAll());
        description = "Optergy converted backup file : " + version +" Date: " + dateFormat.format(new Date());
    }


    public void log(String log) {
        System.out.println(log);
    }
    private String getVerionInfo(StringBuffer sum){
           // log(sum.toString());
            String[] summStringArray = sum.toString().split("\r\n");
           // if (summStringArray.length < 4)
            //    return false;

            // Should be of the form: Backup Type: [backuptype]
            String[] backupTypeArray = summStringArray[0].split("\\s");
            //if (backupTypeArray.length != 3)
            //    return false;
            String backupType = backupTypeArray[2];

            //if (!backupType.equals("Job"))
           // {
             //   return false;
            //}
            // See if its a Job with the database option set
//            if (backupType.equals("Job"))
//            {
//                boolean databaseOptionSet = false;
//                for (String line : summStringArray)
//                {
//                    if (line.indexOf("Database") > -1)
//                    {
//                        databaseOptionSet = true;
//                        break;
//                    }
//                }
//                if (databaseOptionSet == false)
//                    return false;
//            }

            // Should be of the form Software Version: [version] (4th line down)
            String[] versionArray = summStringArray[3].split("\\s");
           // if (versionArray.length != 3)
           //     return false;
            String version = versionArray[2];
//            String installedVersion = null;
//            if(Util.isProton())
//            {
//                installedVersion = Util.getVersion(Util.PROTON_PACKAGE_NAME);
//            }
//            else
//            {
//                installedVersion = Util.getVersion(Util.OPTERGY_PACKAGE_NAME);
//            }
//            if (version.equals(installedVersion))
//                return false;

            // If we get here, need to warn that restoring this backup file will most likely destroy the system
            return version;
        }

    private StringBuffer helperBackupVersion(File decrypted){
        StringBuffer backDesc = new StringBuffer();
        StringBuffer summ = new StringBuffer();
        String warning = "false";

        ZipEntry ze;

        byte[] b = new byte[1];

        ZipFile zf = null;
        try
        {
            zf = new ZipFile(decrypted);
            ze = zf.getEntry("description.txt");
            if (ze != null)
            {
                InputStream is = zf.getInputStream(ze);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                while ((is.read(b)) != -1)
                    baos.write(b);
                backDesc.append(baos.toString());
            }
            else
            {
                backDesc.append("Missing description");
            }
            ze = zf.getEntry("summary.txt");
            if (ze != null)
            {
                InputStream is = zf.getInputStream(ze);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                while ((is.read(b)) != -1) {
                    baos.write(b);
                }
                summ.append(baos.toString());
            }
            else
            {
                summ.append("Missing summary");
            }
            zf.close();
        }
        catch (ZipException e)
        {
           log(e.getMessage());
        }
        catch (IOException e)
        {
           log(e.getMessage());
        }
        finally {
            if(decrypted.exists()) {
                //decrypted.delete();
            }
        }
        return summ;
    }
    public void  getBackupVersion(String filepath){
        String version = "0";
        try
        {

            StringBuffer backDesc = new StringBuffer();
            StringBuffer summ = new StringBuffer();
            File f = new File(filepath);
            if (f.exists() == false)
            {
               log("file not found");
            }


            if (f.getName().endsWith("trendlogs.dbf"))
            {
                backDesc.append("System auto generated backup file");
                summ.append("Backup Type: Trendlog \n");
                summ.append("Date Created: " + dateFormat.format(new Date()) + " \n");
                summ.append("Created By: System Auto Backup \n");
                summ.append("Trendlogs: \n");
                summ.append(" Contains all trendlogs at time of backup creation\n");
            }
            else if (!f.getName().endsWith(".gpg"))
            {
                //log("non encrypted backup");
                summ  =  helperBackupVersion(f);
                version = getVerionInfo(summ);
            }
            else
            {
                //decrypt file
                File decrypted = decryptFile(f);

                if(decrypted == null) {
                    //log("invalid encrypted file");
                }else{
                    summ = helperBackupVersion(decrypted);
                }
                version = getVerionInfo(summ);
            }

            //HashMap<String, Object> backupMap = new HashMap<String, Object>();
            //List<String> info = new ArrayList<String>();
            //info.add(summ.toString());
            //info.add(backDesc.toString());
            //info.add(warning);
            //backupMap.put("restore", info);


        }
        finally
        {
            System.out.println(version);
        }
    }

    public void BackupEnergy(String filename) {
        StringBuffer summaryText = new StringBuffer("Backup Type: Energylog \r\nDate Creaoted: " + dateFormat.format(new Date())
                + "\r\nCreated By: Optergy Backup Converter"
                + "\r\nSoftware Version: " + version);
        File energylogBackupFile = createBackupFile(filename, true);
        ZipOutputStream zipStream = null;
        DateFormat dateFormat = LocalisationLookup.getInstance().getShortDateShortTimeFormat();

        try {
            zipStream = new ZipOutputStream(new FileOutputStream(energylogBackupFile));
            zipStream.setLevel(9);
            zipStream.putNextEntry(new ZipEntry("description.txt"));
            zipStream.write(this.description.getBytes());
        } catch (FileNotFoundException e) {
            log(e.getMessage());
            return;
        } catch (IOException e) {
            log(e.getMessage());
            return;
        }

        ArrayList<String> energylogTables = new ArrayList<String>();
        Hashtable<String, String> energylogDescriptions = new Hashtable<String, String>();

        DatabaseControl optergyDB = new DatabaseControl(Databases.OPTERGY);
        DatabaseControl energylogDB = new DatabaseControl(Databases.ENERGYLOGS);

        try {
            // Get list of all energylog data tables in database first.
            Statement st = energylogDB.createStatement();
            ResultSet rs = st.executeQuery("SELECT tablename FROM pg_tables WHERE tablename LIKE 'el_%' except all (select tablename from pg_tables where tablename like '%_aggregate') ORDER BY tablename;");
            while (rs.next()) {
                energylogTables.add(rs.getString("tablename"));
            }
            st.close();

            List<Energylog> energylogs = optergyDB.getBeanDealer().get(Energylog.class);
            for (Energylog energylog : energylogs) {
                energylogDescriptions.put(
                        energylog.getHostDevice() + "_" + energylog.getEnergylogInstance(),
                        energylog.getMeterName()
                                + (energylog.getDescription() != null && !energylog.getDescription().equals("") ? " - " + energylog.getDescription() : "")
                                + " (Energylog base " + energylog.getEnergylogInstance() + ")");
            }

        } catch (Exception e) {
            log(e.getMessage());
        } finally {
            optergyDB.close();
            energylogDB.close();
        }

        log("Selected energylogs");

        // If energylogs list is empty this indicates all energylogs are being backed up
        if (this.energylogs == null)
            this.energylogs = energylogTables;

        ArrayList<String> successList = new ArrayList<String>();
        String errorText = "";
        int currentEnergylogOperation = 0;
        // Backup each energylog
        for (String energylog : this.energylogs) {
            ++currentEnergylogOperation;
            // Energylog tablenames are in the form el_host_instance
            String[] nameParts = energylog.split("_");
            String host = nameParts[1];
            String instance = nameParts[2];

            // Check that data table exists
            if (energylogTables.contains(energylog)) {
                String elDesc = energylogDescriptions.get(host + "_" + instance);
                System.out.println(host + "_" + instance);
                if (elDesc == null || elDesc.equals(""))
                    elDesc = "Unknown meter (Energylog base " + instance + ")";

                // Update status text
                log("(" + currentEnergylogOperation + "/" + this.energylogs.size() + ") - " + elDesc);

                boolean dbBackedup = true;
                try {
                    Runtime rtime = Runtime.getRuntime();
                    Process child = rtime.exec("/bin/bash");
                    BufferedWriter outCommand = new BufferedWriter(new OutputStreamWriter(child.getOutputStream()));
                    outCommand.write("sudo -u optergy /usr/local/Optergy/bin/scripts/backupDataTables.sh energylogs " + energylog);
                    //If New Reports Framework add the aggregate tables as well
                    //backupAggregateTables
                    outCommand.flush();
                    outCommand.close();

                    try {
                        child.waitFor();
                    } catch (InterruptedException e) {
                        log(e.getMessage());
                        dbBackedup = false;
                    }

                    if (child.exitValue() != 0) {
                        log("Exit Code:" + child.exitValue() + " doing Energylog backup");
                        dbBackedup = false;
                    }
                } catch (IOException e) {
                    log(e.getMessage());
                    dbBackedup = false;
                }
                // If energylog table backed up, add file to the zip file
                if (dbBackedup) {
                    zipRecursively(new File("/usr/local/tomcat/temp/" + energylog + ".dbf"), "Energylogs/", zipStream);
                    new File("/usr/local/tomcat/temp/" + energylog + ".dbf").delete();
                    successList.add(elDesc);
                }
                // If here then the backup failed, therefore add energylog to errorText
                else {
                    errorText += ", " + elDesc;
                }
            }
            // If here then the energylog data tbale was not found in Energylog database
            else {
                errorText += ", unknown trendlog base " + nameParts[2];
            }
        }

        summaryText.append("\r\nEnergylogs:");
        Collections.sort(successList, new NaturalOrderComparator());
        for (String s : successList)
            summaryText.append("\r\n\t" + s);

        if (zipStream != null) {
            try {
                // Add summary text
                zipStream.putNextEntry(new ZipEntry("summary.txt"));
                zipStream.write(summaryText.toString().getBytes());
                zipStream.close();
            } catch (Exception e) {
                log(e.getMessage());
            }
        }
        encryptFile(energylogBackupFile);
    }


    public void BackupJob(String filename) {
        StringBuffer summaryText = new StringBuffer("Backup Type: Job Backup  \r\nDate Created : " + dateFormat.format(new Date())
                + "\r\nCreated By: Optergy Backup Converter"
                + "\r\nSoftware Version: " + version);

        File autoBackupDir = new File(Paths.OPTERGY_BACKUPS_PATH);
        if (autoBackupDir.exists() == false) {
            if (autoBackupDir.mkdir() == false) {
                System.out.println("Could not create auto backup folder, aborting.");
                //failedNofication();
                System.exit(1);
            }
        }
        if (autoBackupDir.setWritable(true, true) == false) {
            System.out.println("Could not set permissions on backup folder, aborting.");
            System.exit(2);
        }

        File jobBackupFile = createBackupFile(filename, true);
        ZipOutputStream zipStream = null;
        DateFormat dateFormat = LocalisationLookup.getInstance().getShortDateShortTimeFormat();

        try {
            zipStream = new ZipOutputStream(new FileOutputStream(jobBackupFile));
            zipStream.setLevel(9);
            zipStream.putNextEntry(new ZipEntry("description.txt"));
            zipStream.write(this.description.getBytes());
        } catch (FileNotFoundException e) {
            log(e.getMessage());
            return;
        } catch (IOException e) {
            log(e.getMessage());
            return;
        }

        ArrayList<String> jobContains = new ArrayList<String>();


        log("Optergy Software Configuration Database");
        jobContains.add(JobBackupOptions.DATABASE.getDescription());

        // Backup Optergy database
        if (backupDatabase(Databases.OPTERGY.getDatabaseName(), null)) {
            zipRecursively(new File("/usr/local/tomcat/temp/optergy.dbf"), "Database/", zipStream);
            new File("/usr/local/tomcat/temp/optergy.dbf").delete();
        } else {
            log("SEVERE - System Backup: Optergy Software database backup failed");
        }

        // Backup Issues Database
        if (backupDatabase(Databases.ISSUES.getDatabaseName(), null)) {
            zipRecursively(new File("/usr/local/tomcat/temp/issues.dbf"), "Database/", zipStream);
            new File("/usr/local/tomcat/temp/issues.dbf").delete();
        } else {
            log("SEVERE - System Backup: Issues database backup failed");
        }

        log("Configuration files");
        // Add all property files and serial file
        File[] files = new File("/usr/local/Optergy/conf/").listFiles();
        for (File file : files) {
            if (file.getName().endsWith(".properties"))
                zipRecursively(file, "conf/", zipStream);
            else if (file.getName().equals("serial"))
                zipRecursively(file, "conf/", zipStream);
        }


        log(JobBackupOptions.DDC.getDescription());
        jobContains.add(JobBackupOptions.DDC.getDescription());
        // Add all DDC files
        zipRecursively(new File(Paths.OPTERGY_DDC_PATH), "", zipStream);

        // if (this.jobOptions.contains(JobBackupOptions.AUDIO)) {
        log(JobBackupOptions.AUDIO.getDescription());
        jobContains.add(JobBackupOptions.AUDIO.getDescription());
        // Add all Audio files
        zipRecursively(new File(Paths.OPTERGY_MNS_AUDIO_PATH), "", zipStream);
        zipRecursively(new File(Paths.TOMCAT_AUDIO_PATH), "", zipStream);
        //  }
        // if (this.jobOptions.contains(JobBackupOptions.LANGUAGE)) {
        log(JobBackupOptions.LANGUAGE.getDescription());
        jobContains.add(JobBackupOptions.LANGUAGE.getDescription());
        // Add all language files
        zipRecursively(new File(Paths.OPTERGY_LANGUAGE_PATCH_PATH), "", zipStream);
        //  }
        // if (this.jobOptions.contains(JobBackupOptions.DISPLAYS)) {
        log(JobBackupOptions.DISPLAYS.getDescription());
        jobContains.add(JobBackupOptions.DISPLAYS.getDescription());
        // Add all display files
        zipRecursively(new File(Paths.OPTERGY_DISPLAY_PATH), "", zipStream);
        //  }
        // if (this.jobOptions.contains(JobBackupOptions.IMAGES)) {
        log("Spectrum images");
        jobContains.add("Spectrum images");
        // Add All web images
        log(JobBackupOptions.IMAGES.getDescription());
        jobContains.add(JobBackupOptions.IMAGES.getDescription());
        zipRecursively(new File(Paths.TOMCAT_UNIX_IMAGE_PATH), "", zipStream);
        zipRecursively(new File(Paths.getOptergyPath() + "Overlays"), "", zipStream);
        // }
        //if (this.jobOptions.contains(JobBackupOptions.DOCUMENATION)) {
        log(JobBackupOptions.DOCUMENATION.getDescription());
        jobContains.add(JobBackupOptions.DOCUMENATION.getDescription());
        // Add all documentation files
        zipRecursively(new File(Paths.OPTERGY_WEB_DOCUMENTATION_PATH), "", zipStream);
        zipRecursively(new File("/usr/local/Optergy/Documentation"), "", zipStream);
        //  }
        // if (this.jobOptions.contains(JobBackupOptions.LICENCE)) {
       /* removed for testing */
        /*
        log(JobBackupOptions.LICENCE.getDescription());
        jobContains.add(JobBackupOptions.LICENCE.getDescription());
        // Add all licence files
        File[] files_conf = new File("/usr/local/Optergy/conf/").listFiles();
        for (File file : files_conf) {
            if (file.getName().startsWith("optergyLicence") && file.getName().endsWith(".gpg"))
                zipRecursively(file, "conf/", zipStream);
        }
        */
        // }

        log("Summary");
        summaryText.append("\r\nJob Options:");
        for (String s : jobContains) {
            summaryText.append("\r\n\t" + s);
        }

        if (zipStream != null) {
            try {
                // Add summary text
                zipStream.putNextEntry(new ZipEntry("summary.txt"));
                zipStream.write(summaryText.toString().getBytes());
                zipStream.close();
            } catch (Exception e) {
                log(e.getMessage());
            }
        }
        encryptFile(jobBackupFile);
    }


    private File createBackupFile(String backupFilename, boolean create) {
        File f;
        f = new File("/usr/local/Optergy/Backup/" + backupFilename);
        if (f.exists()) {
            if (f.delete() == false) {
                System.out.println("File already exists, could not delete, aborting.");
                return null;
            }
        }
        if (create) {
            try {
                log(f.getName());
                f.createNewFile();
                f.setWritable(true, false);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
        return f;
    }

    private boolean backupDatabase(String database, String location) {
        boolean dbBackupResult = true;

        try {
            Runtime rtime = Runtime.getRuntime();
            Process child = rtime.exec("/bin/bash");
            BufferedWriter outCommand = new BufferedWriter(new OutputStreamWriter(child.getOutputStream()));
            outCommand.write("sudo -u optergy /usr/local/Optergy/bin/scripts/backupdb.sh " + database + (location != null ? " " + location : ""));
            outCommand.flush();
            outCommand.close();

            try {
                child.waitFor();
            } catch (InterruptedException e) {
                log(e.getMessage());
                dbBackupResult = false;
            }

            if (child.exitValue() != 0) {
                dbBackupResult = false;
                log(location + " Bad Exit code:" + child.exitValue());
            }

        } catch (IOException e) {
            log(e.getMessage());
            dbBackupResult = false;
        }

        return dbBackupResult;
    }

    private void zipRecursively(File f, String path, ZipOutputStream z) {
        try {
            if (!f.exists()) {
                System.out.println("File not found:" + f);
                return;
            }
            if (!f.isDirectory()) {
                z.putNextEntry(new ZipEntry((path + f.getName())));
                FileInputStream in = new FileInputStream(f);
                byte[] b = new byte[512];
                int len = 0;
                while ((len = in.read(b)) != -1)
                    z.write(b, 0, len);
                z.closeEntry();
                in.close();
                f = null;
            } else
            // recurse
            {
                String newFolder = path + f.getName() + '/';

                z.putNextEntry(new ZipEntry(newFolder));
                z.closeEntry();
                String[] folderListing = f.list();
                for (int i = 0; i < folderListing.length; i++) {
                    zipRecursively(new File(f.getAbsolutePath() + "/" + folderListing[i]), newFolder, z);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //trendlog backup
    public void BackupTrend(String filename) {
        try {
            log("Creating full trendlog backup. This backup type may take quite some time.");
            Util.executeUnixCommand("sudo -u optergy /home/optergy/bins/fullTrendlogBackup.sh 'Backup Genarated By Optergy :"+ this.version +"' 'Optergy' '"+filename+"'", "Performing Full Manual Trendlog Backup", null);
            //encryptTrendlogs();

        } catch (InterruptedException | IOException | PlatformException | ErrorCodeException e) {
            log(e.getMessage());
            log("Failed to perform Full Trendlog Manual Backup");
        }
    }

    private void deleteBackups() {
        File backupDir = new File(Paths.OPTERGY_BACKUPS_PATH);
        File[] trendlogBackFiles = null; //list all files

        for (File file : trendlogBackFiles) {
            if (file.getName().contains(".backup")) {
                file.delete();
            }
        }
    }

    private void encryptTrendlogs() {
        File backupDir = new File(Paths.OPTERGY_BACKUPS_PATH);
        File[] trendlogBackFiles = backupDir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
               return name.startsWith("Trendlog");
            }
        });
        //Encrypt all parts
        if (trendlogBackFiles != null)
            for (File file : trendlogBackFiles) {
                encryptFile(file);
            }
    }
    public File decryptFile(File f) {
        try
        {
            int gpgVersion = Integer.parseInt(Util.executeUnixCommand("sudo gpg --version | grep '(GnuPG)' | sed 's/.*)//' | sed 's/ //g'", "obtaining gpg version", null).split("\\.")[0]);
            if (gpgVersion < 2) {
                Util.executeUnixCommandIgnoreError("sudo gpg --passphrase test --decrypt --no-tty --output " + "\"" + f.getAbsolutePath().split("\\.")[0] + ".backup\" "
                        + "\"" + f.getAbsolutePath() + "\"", "encrypting file", null);
                File decrypted = new File(f.getAbsolutePath().split("\\.")[0] + ".backup");
                return decrypted;
            }else{
                Util.executeUnixCommandIgnoreError("sudo gpg --passphrase test --decrypt --no-tty --batch --yes --pinentry-mode=loopback --output " + "\"" + f.getAbsolutePath().split("\\.")[0] + ".backup\" "
                        + "\"" + f.getAbsolutePath() + "\"", "encrypting file", null);
                File decrypted = new File(f.getAbsolutePath().split("\\.")[0] + ".backup");
                return decrypted;
            }
        }
        catch (IOException | InterruptedException | PlatformException | ErrorCodeException e)
        {
            return null;
        }
        finally
        {
        }
    }
    //backup
    public void encryptFile(File backupFile) {
        try {
            int gpgVersion = Integer.parseInt(Util.executeUnixCommand("sudo gpg --version | grep '(GnuPG)' | sed 's/.*)//' | sed 's/ //g'", "obtaining gpg version", null).split("\\.")[0]);

            if (gpgVersion < 2) {
                Util.executeUnixCommand("sudo gpg --passphrase test --no-tty --output " + "\"" + backupFile.getAbsolutePath().split("\\.")[0] + ".gpg\"" + " --symmetric "
                        + "\"" + backupFile.getAbsolutePath() + "\"", "encrypting file", null);
            } else {
                Util.executeUnixCommand("sudo gpg --passphrase test --no-tty --batch --yes --pinentry-mode=loopback --output  " + "\"" + backupFile.getAbsolutePath().split("\\.")[0] + ".gpg\"" + " --symmetric "
                        + "\"" + backupFile.getAbsolutePath() + "\"", "encrypting file", null);
            }

        } catch (Exception e) {
            log(e.getMessage());
        } finally {
        }
    }

    public void RestoreBackup(String filename) {

        try {
            // An Auto trendlog backup (pg_dump)
            File backupDir = new File(Paths.OPTERGY_BACKUPS_PATH+filename);
            if(backupDir.getName().contains("gpg")) { // check if file need to encrypt
                backupDir = decryptFile(backupDir);
            }
            this.f = backupDir;
            if (this.f.getName().endsWith("trendlogs.dbf")) {
                log("Optergy Software Trendlogs");
                Runtime rtime = Runtime.getRuntime();
                Process child = rtime.exec("/bin/bash");
                BufferedWriter outCommand = new BufferedWriter(new OutputStreamWriter(child.getOutputStream()));
                // NOTE: For some reason the postgres call within this script will not return unless we pipe out the
                // syserr
                String cmd = "sudo -u optergy /usr/local/Optergy/bin/scripts/restoredb.sh " + this.f.getPath() + " " + Databases.TRENDLOGS.getDatabaseName()
                        + " &>> /usr/local/Optergy/logs/DatabaseRestore.log ";
                outCommand.write(cmd);
                outCommand.flush();
                outCommand.close();

                BufferedReader br = new BufferedReader(new InputStreamReader(child.getInputStream()));
                String str;
                while ((str = br.readLine()) != null) {
                    log(str);
                }
                br = new BufferedReader(new InputStreamReader(child.getErrorStream()));
                while ((str = br.readLine()) != null) {
                    log(str);
                }
                try {
                    child.waitFor();
                } catch (InterruptedException e1) {
                    // ignore
                }

                // TODO update this once postgres no longer throws an error when restoring databases
                if (child.exitValue() > 1) {
                    log("Error restoring Trendlog database, error code: " + child.exitValue());
                    log("Error restoring Trendlog database");
                }
            }
            // Presume it is a normal .backup file
            else {

                ArrayList<String> errorTrendlogTables = new ArrayList<String>();
                ArrayList<String> errorEnergylogTables = new ArrayList<String>();

                boolean monitStopped = false;
                boolean meteringStopped = false;
                // Default permission mask for newly created file: 644
                ZipFile zf = new ZipFile(this.f);
                Enumeration<?> e = zf.entries();
                Util.executeUnixCommand("cp /usr/local/Optergy/conf/webserver.properties /usr/local/Optergy/conf/webserver.backup", "Backing up the Webserver conf file to restore later.", null);
                while (e.hasMoreElements()) {
                    ZipEntry ze = (ZipEntry) e.nextElement();

                    log("Name: " + ze.getName());
                    if (ze.getName().equals("Database/optergy.dbf")) {
                        try {
                            // Stop Optergy processes, if fails do not restore optergy backup
                            String response = Util.executeUnixCommand("sudo invoke-rc.d optergy stop", "System Restore: Stop Optergy Software", null);

                            String path = "/usr/local/tomcat/temp/optergy.dbf";
                            extractZipEntryToFile(path, zf, ze);
                            //SetProgressStatusText("Optergy Software Configuration Database");
                            Runtime rtime = Runtime.getRuntime();
                            Process child = rtime.exec("/bin/bash");
                            BufferedWriter outCommand = new BufferedWriter(new OutputStreamWriter(child.getOutputStream()));
                            // NOTE: For some reason the postgres call within this script will not return unless we pipe
                            // out the syserr
                            String cmd = "sudo -u optergy /usr/local/Optergy/bin/scripts/restoredb.sh " + path + " " + Databases.OPTERGY.getDatabaseName()
                                    + " > /usr/local/Optergy/logs/databaseRestore.log 2>&1";
                            outCommand.write(cmd);
                            outCommand.flush();
                            outCommand.close();

                            BufferedReader br = new BufferedReader(new InputStreamReader(child.getInputStream()));
                            String str;
                            while ((str = br.readLine()) != null) {
                                log(str);
                            }
                            br = new BufferedReader(new InputStreamReader(child.getErrorStream()));
                            while ((str = br.readLine()) != null) {
                                log(str);
                            }
                            try {
                                child.waitFor();
                            } catch (InterruptedException e1) {
                                // ignore
                            }

                            // TODO update this once postgres no longer throws an error when restoring databases
                            if (child.exitValue() > 1) {
                                log("Error restoring Optergy Software databases, error code: " + child.exitValue());
                            }
                        } catch (InterruptedException e2) {
                            log("Error stopping Optergy Software in order to restore Optergy Software databases");
                        } catch (PlatformException e3) {
                            log("Error stopping Optergy Software in order to restore Optergy Software databases");
                            this.msgText += ERROR_RESTORING_AURORA_DB;
                        } catch (ErrorCodeException e4) {
                            log("Error stopping Optergy Software in order to restore Optergy Software databases");
                        }
                        // If optergy DB is restored without issues, we should validate the license file.
                        //if (msgText.indexOf(ERROR_RESTORING_AURORA_DB) == -1)
                        // {
                        //     shouldValidateLicence = true;
                        // }
                        // new File(path).delete();
                    } else if (ze.getName().equals("Database/issues.dbf")) {
                        String path = "/usr/local/tomcat/temp/issues.dbf";
                        extractZipEntryToFile(path, zf, ze);
                        //SetProgressStatusText("Optergy Software Configuration Database");
                        Runtime rtime = Runtime.getRuntime();
                        Process child = rtime.exec("/bin/bash");
                        BufferedWriter outCommand = new BufferedWriter(new OutputStreamWriter(child.getOutputStream()));
                        // NOTE: For some reason the postgres call within this script will not return unless we pipe out
                        // the syserr
                        String cmd = "sudo -u optergy /usr/local/Optergy/bin/scripts/restoredb.sh " + path + " " + Databases.ISSUES.getDatabaseName()
                                + " > /usr/local/Optergy/logs/databaseRestore.log 2>&1";
                        outCommand.write(cmd);
                        outCommand.flush();
                        outCommand.close();

                        BufferedReader br = new BufferedReader(new InputStreamReader(child.getInputStream()));
                        String str;
                        while ((str = br.readLine()) != null) {
                            log(str);
                        }
                        br = new BufferedReader(new InputStreamReader(child.getErrorStream()));
                        while ((str = br.readLine()) != null) {
                            log(str);
                        }
                        try {
                            child.waitFor();
                        } catch (InterruptedException e1) {
                            // ignore
                        }

                        // TODO update this once postgres no longer throws an error when restoring databases
                        if (child.exitValue() > 1) {
                            log("Error restoring Issues database, error code: " + child.exitValue());
                        }

                        // new File(path).delete();
                    } else if (ze.getName().startsWith("ddc/")) {
                        log("Programming Tool");
                        String path = "/usr/local/Optergy/data/";
                        extractFile(path, zf, ze);
                    } else if (ze.getName().startsWith("audioFiles/")) {
                        //log("Audio Files");
                        String path = "/usr/local/Optergy/data/";
                        extractFile(path, zf, ze);
                    } else if (ze.getName().startsWith("audio/")) {
                        log("Tomcat Audio");
                        String path = "/usr/local/tomcat/webapps/ROOT/";
                        extractFile(path, zf, ze);
                    } else if (ze.getName().startsWith("languagePatches/")) {
                        log("Language Patches");
                        String path = "/usr/local/Optergy/data/";
                        extractFile(path, zf, ze);
                    } else if (ze.getName().startsWith("Displays/")) {
                        log("Displays");
                        String path = Paths.getOptergyPath();
                        extractFile(path, zf, ze);
                    } else if (ze.getName().startsWith("images/")) {
                        log("Images");
                        String path = "/usr/local/tomcat/webapps/ROOT/";
                        extractFile(path, zf, ze);
                    } else if (ze.getName().startsWith("Overlays/")) {
                        if (ze.getName().endsWith("transparent.gif"))
                            continue;
                        log("Spectrum images");
                        String path = Paths.getOptergyPath();
                        extractFile(path, zf, ze);
                    } else if (ze.getName().startsWith("help/")) {
                        log("HTML documents");
                        String path = "/usr/local/tomcat/webapps/ROOT/";
                        extractFile(path, zf, ze);
                    } else if (ze.getName().startsWith("Documentation/")) {
                        log("Documentation");
                        String path = Paths.getOptergyPath();
                        extractFile(path, zf, ze);
                    } else if (ze.getName().startsWith("conf/")) {
                        log("Configuration files");
                        String path = Paths.getOptergyPath();
                        extractFile(path, zf, ze);
                        if (ze.isDirectory() == false) {
                            Util.setFilePermission(path + ze.getName(), "660");
                            Util.setFileOwner("optergy", path + ze.getName());
                        }
                    } else if (ze.getName().startsWith("Trendlogs/")) {
                        String path = Paths.TOMCAT_TEMP_PATH;
                        String filename_ = ze.getName().substring(ze.getName().lastIndexOf("/") + 1);
                        String bkupTable = filename_.replace(".dbf", "");
                        log("Trendlog data (" + bkupTable.replace("temp", "") + ")");
                        extractZipEntryToFile(path + filename_, zf, ze);

                        // Do a check to see if the table already exists, if so, provide the -a option to the restore
                        // script
                        boolean dataOnly = false;
                        WebDatabaseControl dbTrendControl = new WebDatabaseControl(Databases.TRENDLOGS);
                        try {
                            Statement st = dbTrendControl.createStatement();
                            ResultSet rs = st.executeQuery("SELECT * FROM pg_tables WHERE tablename = " + Util.escapeAndQuote(bkupTable) + "");
                            if (rs.next()) {
                                dataOnly = true;
                            }
                            st.close();
                        } catch (SQLException e1) {
                            log(e1.getSQLState());
                        }

                        Runtime rtime = Runtime.getRuntime();
                        Process child = rtime.exec("/bin/bash");
                        BufferedWriter outCommand = new BufferedWriter(new OutputStreamWriter(child.getOutputStream()));
                        String cmd = "sudo -u optergy /usr/local/Optergy/bin/scripts/restoreDataTable.sh " + Databases.TRENDLOGS.getDatabaseName() + " "
                                + filename + " " + bkupTable + " " + (dataOnly ? "-a" : "");
                        outCommand.write(cmd);
                        outCommand.flush();
                        outCommand.close();


                        BufferedReader br = new BufferedReader(new InputStreamReader(child.getInputStream()));
                        String str;
                        while ((str = br.readLine()) != null) {
                            log(str);
                        }
                        br = new BufferedReader(new InputStreamReader(child.getErrorStream()));
                        while ((str = br.readLine()) != null) {
                            log(str);
                        }
                        try {
                            child.waitFor();
                        } catch (InterruptedException e1) {
                        }

                        if (child.exitValue() != 0) {
                            log("Error restoring Trendlogs database table " + bkupTable + ", error code: " + child.exitValue());
                            // drop existing table, keep a copy, restore table and then copy old data

                            if (!errorTrendlogTables.contains(bkupTable.replace("temp", "")))
                                errorTrendlogTables.add(bkupTable.replace("temp", ""));
                        }

                        String tableToVacuum = bkupTable;
                        // If filename (tablename) ends with temp, copy the data into tablename[-temp] and then drop the
                        // table
                        if (bkupTable.endsWith("temp")) {
                            String restoreToTable = bkupTable.replace("temp", "");
                            tableToVacuum = restoreToTable;
                            log("Restore to this table: " + restoreToTable);
                            // If dataOnly == false, the original table does not exist (most likely restoring to a fresh
                            // system, in
                            // which case, alter table instead of copying data
                            // SEE IF restoreToTable exists: if it does, copy the data from the temp table to
                            // restoreToTable,
                            // if not, do ALTER TABLE
                            try {
                                Statement st = dbTrendControl.createStatement();
                                ResultSet rs = st.executeQuery("SELECT * FROM pg_tables WHERE tablename = " + Util.escapeAndQuote(restoreToTable) + "");
                                if (rs.next()) {
                                    // This inner select will make sure we are not insert records that already exist
                                    String innerSelect = "SELECT " + bkupTable + ".* FROM " + bkupTable + " LEFT JOIN " + restoreToTable + " ON " + bkupTable
                                            + ".id = " +
                                            restoreToTable + ".id WHERE " + restoreToTable + ".id IS NULL";
                                    st.executeUpdate("INSERT INTO " + restoreToTable + " " + innerSelect);
                                    st.executeUpdate("DROP TABLE " + bkupTable);
                                } else {
                                    st.execute("ALTER TABLE \"" + bkupTable + "\" RENAME TO \"" + restoreToTable + "\"");
                                }
                            } catch (SQLException e1) {
                                log(e1.getSQLState());
                                if (!errorTrendlogTables.contains(restoreToTable))
                                    errorTrendlogTables.add(restoreToTable);
                            }
                        }
                        try {
                            Statement st = dbTrendControl.createStatement();
                            st.execute("VACUUM FULL \"" + tableToVacuum + "\"");
                        } catch (SQLException e1) {
                            log(e1.getSQLState());
                        }
                        Runtime run = Runtime.getRuntime();
                        Process running = run.exec("/bin/bash");
                        BufferedWriter outofCommands = new BufferedWriter(new OutputStreamWriter(running.getOutputStream()));
                        String console = "sudo -u optergy /usr/local/Optergy/bin/scripts/sequenceaddition.sh " + bkupTable;
                        outofCommands.write(console);
                        outofCommands.flush();
                        outofCommands.close();
                        BufferedReader brr = new BufferedReader(new InputStreamReader(running.getInputStream()));
                        String strr;
                        while ((strr = brr.readLine()) != null) {
                            log(strr);
                        }
                        br = new BufferedReader(new InputStreamReader(running.getErrorStream()));
                        while ((strr = brr.readLine()) != null) {
                            log(strr);
                        }
                        try {
                            running.waitFor();
                        } catch (InterruptedException e1) {
                        }


                        dbTrendControl.close();

                        new File(path + filename).delete();
                    } else if (ze.getName().startsWith("Energylogs/")) {
                        log("Energylog data");
                        String path = "/usr/local/tomcat/temp/";
                        String filename__ = ze.getName().substring(ze.getName().lastIndexOf("/") + 1);
                        String bkupTable = filename__.replace(".dbf", "");
                        extractZipEntryToFile(path + filename__, zf, ze);
                        boolean failed = false;
                        // Make sure monit/metering is stopped
                        if (monitStopped == false) {
                            try {
                                String response = Util.executeUnixCommand("sudo invoke-rc.d monit stop", "System Restore: Stop Monit", null);
                                if (response == null) {
                                    failed = true;
                                }
                            } catch (Exception ex) {
                                log("Error stopping Monit in order to restore Energylog table");
                            } finally {
                                if (failed) {
                                    errorEnergylogTables.add(bkupTable);
                                    continue;
                                }
                                monitStopped = true;
                            }
                        }

                        if (meteringStopped == false) {
                            try {
                                String response = Util.executeUnixCommand("sudo invoke-rc.d metering stop", "System Restore: Stop Metering", null);
                                if (response == null) {
                                    failed = true;
                                }
                                // Delete the meter reading dat files so that metering picks up from whats in the database
                                String dataFilePath = "/usr/local/Optergy/data/metering/energyreadings/";
                                File folder = new File(dataFilePath);
                                FileUtils.cleanDirectory(folder);
                            } catch (Exception ex) {
                                log("Error stopping metering in order to restore Energylog table");
                                this.msgText += filename;
                            } finally {
                                if (failed) {
                                    errorEnergylogTables.add(bkupTable);
                                    continue;
                                }
                                meteringStopped = true;
                            }
                        }

                        // Do a check to see if the table already exists, if so, drop the table
                        WebDatabaseControl dbEnergylogControl = new WebDatabaseControl(Databases.ENERGYLOGS);
                        try {
                            Statement st = dbEnergylogControl.createStatement();
                            ResultSet rs = st.executeQuery("SELECT * FROM pg_tables WHERE tablename = " + Util.escapeAndQuote(bkupTable) + "");
                            if (rs.next()) {
                                st.execute("DROP TABLE \"" + bkupTable + "\"");
                            }
                            rs = st.executeQuery("SELECT * FROM pg_tables WHERE tablename = " + Util.escapeAndQuote(bkupTable) + "_aggregate" + "");
                            if (rs.next()) {
                                st.execute("DROP TABLE \"" + bkupTable + "_aggregate" + "\"");
                            }
                            st.close();
                        } catch (SQLException e1) {
                            log(e1.getSQLState());
                        }

                        Runtime rtime = Runtime.getRuntime();
                        Process child = rtime.exec("/bin/bash");
                        BufferedWriter outCommand = new BufferedWriter(new OutputStreamWriter(child.getOutputStream()));
                        String cmd = "sudo -u optergy /usr/local/Optergy/bin/scripts/restoreDataTable.sh " + Databases.ENERGYLOGS.getDatabaseName() + " "
                                + filename + " " + bkupTable;
                        outCommand.write(cmd);
                        outCommand.flush();
                        outCommand.close();

                        BufferedReader br = new BufferedReader(new InputStreamReader(child.getInputStream()));
                        String str;
                        while ((str = br.readLine()) != null) {
                            log(str);
                        }
                        br = new BufferedReader(new InputStreamReader(child.getErrorStream()));
                        while ((str = br.readLine()) != null) {
                            log(str);
                        }
                        try {
                            child.waitFor();
                        } catch (InterruptedException e1) {
                        }

                        if (child.exitValue() != 0) {
                            log("Error restoring EnergyLogs databases, error code: " + child.exitValue());
                            errorEnergylogTables.add(bkupTable);
                        }

                        try {
                            Statement st = dbEnergylogControl.createStatement();
                            st.execute("VACUUM FULL \"" + bkupTable + "\"");
                        } catch (SQLException e1) {
                            log(e1.getSQLState());
                        }

                        dbEnergylogControl.close();
                        new File(path + filename).delete();
                    } else {
                        if (ze.getName().equals("description.txt") || ze.getName().equals("summary.txt"))
                            continue;
                        log("Unknown file: :" + ze.getName());
                    }
                }

                if (!errorEnergylogTables.isEmpty()) {
                    this.msgText += ", error restoring energylog ";
                    for (String table : errorEnergylogTables)
                        this.msgText += table + ", ";
                }
                if (!errorTrendlogTables.isEmpty()) {
                    this.msgText += ", error restoring trendlog ";
                    for (String table : errorTrendlogTables)
                        this.msgText += table + ", ";
                }

                if (meteringStopped) {
                    // Attempt to start Metering, if failed report back to screen
                    try {
                        String response = Util.executeUnixCommand("sudo invoke-rc.d metering start", "System Restore: Start Metering", null);
                        if (response == null)
                            throw new Exception();
                    } catch (Exception exec) {
                        log("Error starting Metering");
                        this.msgText += ",  Error starting Optergy Software (metering), Portal must be rebooted";
                    }
                }
            }

            sanitiseTrendlogDescriptions();

            // Attempt to start Optergy, if failed report back to screen
            try {

                // Update the database revision number
                File db_revision_file = new File("/usr/local/Optergy/data/database-revision");
                try {
                    db_revision_file.createNewFile();
                    int count = 0;
                    List<String> lines = Files.readAllLines(java.nio.file.Paths.get("/usr/local/Optergy/data/database-revision"), Charset.forName("US-ASCII"));
                    if (lines.size() > 0) {
                        count = Integer.parseInt(lines.get(0));
                    }
                    count++;
                    PrintWriter writer = new PrintWriter("/usr/local/Optergy/data/database-revision");
                    writer.println(count);
                    writer.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

                String response = Util.executeUnixCommand("sudo invoke-rc.d optergy start", "System Restore: Start Optergy Software", null);
                log(response);
            } catch (Exception exec) {
                log("Error starting Optergy Software, Portal must be rebooted");
            }

            //need to read the webserver.backup file and put the kiosk mode values back into the working file
            //then delete the backup
            try {
                String BACKUP = "/usr/local/Optergy/conf/webserver.backup";
                String INUSE = ("/usr/local/Optergy/conf/webserver.properties");
                FileReader fileReader = new FileReader(BACKUP);
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                String currentLine;
                String kioskStatus = null;
                ArrayList<String> replacementList = new ArrayList();
                while ((currentLine = bufferedReader.readLine()) != null) {
                    if (currentLine.contains("enable_kiosk_mode")) {
                        kioskStatus = currentLine;
                    }
                }
                //now need to read in the current file and replace the enable_kiosk_mode string
                fileReader = new FileReader(INUSE);
                bufferedReader = new BufferedReader(fileReader);
                boolean hasBeenAdded = false;
                while ((currentLine = bufferedReader.readLine()) != null) {
                    if (currentLine.contains("enable_kiosk_mode")) {
                        currentLine = kioskStatus;
                        hasBeenAdded = true;
                    }
                    replacementList.add(currentLine);
                }
                bufferedReader.close();
                //this will happen if there is no enable_kiosk_mode line in the new file.
                if (!hasBeenAdded) {
                    replacementList.add(kioskStatus);
                }
                //now delete the conf file and then write out this list to it.
                File oldConfFile = new File(INUSE);
                File backupFile = new File(BACKUP);
                if (oldConfFile.exists()) {
                    oldConfFile.delete();
                }
                if (backupFile.exists()) {
                    backupFile.delete();
                }

                PrintWriter writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(oldConfFile, true), "UTF-8"));
                for (String output : replacementList) {
                    writer.println(output);
                }
                writer.close();
            } catch (Exception e) {
                log(e.getMessage());
            }

        } catch (IOException e) {
            log(e.getMessage());
        } catch (InterruptedException e5) {
            // TODO Auto-generated catch block
            e5.printStackTrace();
        } catch (PlatformException e5) {
            // TODO Auto-generated catch block
            e5.printStackTrace();
        } catch (ErrorCodeException e5) {
            // TODO Auto-generated catch block
            e5.printStackTrace();
        } finally {
            if (this.f.exists())
                f.delete();
        }

        boolean restartNeeded = (this.msgText == null || this.msgText.length() < 1) && shouldValidateLicence;

        // Finished notification
        try {
            // create system message event and flag as notification
            // String = "System Restore of " will be used by session-tasks.js to PAUSE sending AJAX requests as Tomcat
            // won't be available for a while.
            if (this.msgText == null || this.msgText.length() < 1)
                log("System Restore of  " + f.getName()
                        + " finished successfully");
            else
                log("System Restore of " + f.getName()
                        + " experienced problems" + this.msgText);
        } catch (Exception e1) {
            log(e1.getMessage());
        }

        // Validate license file just unpacked from backup & remove license features if validation fails.
        if (shouldValidateLicence) {
            this.validateLicence();
        }

        log("Restore process completed successfully, Licence check is = " + shouldValidateLicence);
        log("Restore process is all done: Tomcat restartNeeded = " + restartNeeded);

        if (restartNeeded) {
            this.restartTomcat();
        }
    }
/*



    public void Restore(String filename_)
    {
        try
        {
            File f = new File(Paths.OPTERGY_BACKUPS_PATH + filename_);
            File decrypted = decryptFile(f);
            this.f = decrypted;
            // An Auto trendlog backup (pg_dump)
            if (this.f.getName().endsWith("trendlogs.dbf"))
            {
                log("Optergy Software Trendlogs");
                Runtime rtime = Runtime.getRuntime();
                Process child = rtime.exec("/bin/bash");
                BufferedWriter outCommand = new BufferedWriter(new OutputStreamWriter(child.getOutputStream()));
                // NOTE: For some reason the postgres call within this script will not return unless we pipe out the
                // syserr
                String cmd = "sudo -u optergy /usr/local/Optergy/bin/scripts/restoredb.sh " + this.f.getPath() + " " + Databases.TRENDLOGS.getDatabaseName()
                        + " &>> /usr/local/Optergy/logs/DatabaseRestore.log ";
                outCommand.write(cmd);
                outCommand.flush();
                outCommand.close();

                BufferedReader br = new BufferedReader(new InputStreamReader(child.getInputStream()));
                String str;
                while ((str = br.readLine()) != null)
                {
                   log( str);
                }
                br = new BufferedReader(new InputStreamReader(child.getErrorStream()));
                while ((str = br.readLine()) != null)
                {
                    log( str);
                }
                try
                {
                    child.waitFor();
                }
                catch (InterruptedException e1)
                {
                    // ignore
                }

                // TODO update this once postgres no longer throws an error when restoring databases
                if (child.exitValue() > 1)
                {
                    log( "Error restoring Trendlog database, error code: " + child.exitValue());
                    this.msgText += ", error restoring Trendlog database";
                }
            }
            // Presume it is a normal .backup file
            else
            {

                ArrayList<String> errorTrendlogTables = new ArrayList<String>();
                ArrayList<String> errorEnergylogTables = new ArrayList<String>();

                boolean monitStopped = false;
                boolean meteringStopped = false;
                // Default permission mask for newly created file: 644
                ZipFile zf = new ZipFile(this.f);
                Enumeration<?> e = zf.entries();
                Util.executeUnixCommand("cp /usr/local/Optergy/conf/webserver.properties /usr/local/Optergy/conf/webserver.backup", "Backing up the Webserver conf file to restore later.",null);
                while (e.hasMoreElements())
                {
                    ZipEntry ze = (ZipEntry) e.nextElement();

                    log("Name: " + ze.getName());
                    if (ze.getName().equals("Database/optergy.dbf"))
                    {
                        try
                        {
                            // Stop Optergy processes, if fails do not restore optergy backup
                            String response = Util.executeUnixCommand("sudo invoke-rc.d optergy stop", "System Restore: Stop Optergy Software", null);

                            String path = "/usr/local/tomcat/temp/optergy.dbf";
                            extractZipEntryToFile(path, zf, ze);
                            log("Optergy Software Configuration Database");
                            Runtime rtime = Runtime.getRuntime();
                            Process child = rtime.exec("/bin/bash");
                            BufferedWriter outCommand = new BufferedWriter(new OutputStreamWriter(child.getOutputStream()));
                            // NOTE: For some reason the postgres call within this script will not return unless we pipe
                            // out the syserr
                            String cmd = "sudo -u optergy /usr/local/Optergy/bin/scripts/restoredb.sh " + path + " " + Databases.OPTERGY.getDatabaseName()
                                    + " > /usr/local/Optergy/logs/databaseRestore.log 2>&1";
                            outCommand.write(cmd);
                            outCommand.flush();
                            outCommand.close();

                            BufferedReader br = new BufferedReader(new InputStreamReader(child.getInputStream()));
                            String str;
                            while ((str = br.readLine()) != null)
                            {
                              log( str);
                            }
                            br = new BufferedReader(new InputStreamReader(child.getErrorStream()));
                            while ((str = br.readLine()) != null)
                            {
                               log( str);
                            }
                            try
                            {
                                child.waitFor();
                            }
                            catch (InterruptedException e1)
                            {
                                // ignore
                            }

                            // TODO update this once postgres no longer throws an error when restoring databases
                            if (child.exitValue() > 1)
                            {
                               log( "Error restoring Optergy Software databases, error code: " + child.exitValue());
                                this.msgText += ERROR_RESTORING_AURORA_DB;
                            }
                        }
                        catch (InterruptedException e2)
                        {
                          log("Error stopping Optergy Software in order to restore Optergy Software databases");
                            this.msgText += ERROR_RESTORING_AURORA_DB;
                        }
                        catch (PlatformException e3)
                        {
                            log("Error stopping Optergy Software in order to restore Optergy Software databases");
                            this.msgText += ERROR_RESTORING_AURORA_DB;
                        }
                        catch (ErrorCodeException e4)
                        {
                            log( "Error stopping Optergy Software in order to restore Optergy Software databases");
                            this.msgText += ERROR_RESTORING_AURORA_DB;
                        }
                        // If optergy DB is restored without issues, we should validate the license file.
                        if (msgText.indexOf(ERROR_RESTORING_AURORA_DB) == -1)
                        {
                            shouldValidateLicence = true;
                        }
                        // new File(path).delete();
                    }
                    else if (ze.getName().equals("Database/issues.dbf"))
                    {
                        String path = "/usr/local/tomcat/temp/issues.dbf";
                        extractZipEntryToFile(path, zf, ze);
                        log("Optergy Software Configuration Database");
                        Runtime rtime = Runtime.getRuntime();
                        Process child = rtime.exec("/bin/bash");
                        BufferedWriter outCommand = new BufferedWriter(new OutputStreamWriter(child.getOutputStream()));
                        // NOTE: For some reason the postgres call within this script will not return unless we pipe out
                        // the syserr
                        String cmd = "sudo -u optergy /usr/local/Optergy/bin/scripts/restoredb.sh " + path + " " + Databases.ISSUES.getDatabaseName()
                                + " > /usr/local/Optergy/logs/databaseRestore.log 2>&1";
                        outCommand.write(cmd);
                        outCommand.flush();
                        outCommand.close();

                        BufferedReader br = new BufferedReader(new InputStreamReader(child.getInputStream()));
                        String str;
                        while ((str = br.readLine()) != null)
                        {
                           log( str);
                        }
                        br = new BufferedReader(new InputStreamReader(child.getErrorStream()));
                        while ((str = br.readLine()) != null)
                        {
                            log(str);
                        }
                        try
                        {
                            child.waitFor();
                        }
                        catch (InterruptedException e1)
                        {
                            // ignore
                        }

                        // TODO update this once postgres no longer throws an error when restoring databases
                        if (child.exitValue() > 1)
                        {
                           log( "Error restoring Issues database, error code: " + child.exitValue());
                            this.msgText += ", error restoring Issues database";
                        }

                        // new File(path).delete();
                    }
                    else if (ze.getName().startsWith("ddc/"))
                    {
                        log("Programming Tool");
                        String path = "/usr/local/Optergy/data/";
                        extractFile(path, zf, ze);
                    }
                    else if (ze.getName().startsWith("audioFiles/"))
                    {
                        log("Audio Files");
                        String path = "/usr/local/Optergy/data/";
                        extractFile(path, zf, ze);
                    }
                    else if (ze.getName().startsWith("audio/"))
                    {
                        log("Tomcat Audio");
                        String path = "/usr/local/tomcat/webapps/ROOT/";
                        extractFile(path, zf, ze);
                    }
                    else if (ze.getName().startsWith("languagePatches/"))
                    {
                        log("Language Patches");
                        String path = "/usr/local/Optergy/data/";
                        extractFile(path, zf, ze);
                    }
                    else if (ze.getName().startsWith("Displays/"))
                    {
                        log("Displays");
                        String path = Paths.getOptergyPath();
                        extractFile(path, zf, ze);
                    }
                    else if (ze.getName().startsWith("images/"))
                    {
                        log("Images");
                        String path = "/usr/local/tomcat/webapps/ROOT/";
                        extractFile(path, zf, ze);
                    }
                    else if (ze.getName().startsWith("Overlays/"))
                    {
                        if (ze.getName().endsWith("transparent.gif"))
                            continue;
                        log("Spectrum images");
                        String path = Paths.getOptergyPath();
                        extractFile(path, zf, ze);
                    }
                    else if (ze.getName().startsWith("help/"))
                    {
                        log("HTML documents");
                        String path = "/usr/local/tomcat/webapps/ROOT/";
                        extractFile(path, zf, ze);
                    }
                    else if (ze.getName().startsWith("Documentation/"))
                    {
                        log("Documentation");
                        String path = Paths.getOptergyPath();
                        extractFile(path, zf, ze);
                    }
                    else if (ze.getName().startsWith("conf/"))
                    {
                        log("Configuration files");
                        String path = Paths.getOptergyPath();
                        extractFile(path, zf, ze);
                        if (ze.isDirectory() == false)
                        {
                            Util.setFilePermission(path + ze.getName(), "660");
                            Util.setFileOwner("optergy", path + ze.getName());
                        }
                    }
                    else if (ze.getName().startsWith("Trendlogs/"))
                    {
                        String path = Paths.TOMCAT_TEMP_PATH;
                        String filename = ze.getName().substring(ze.getName().lastIndexOf("/") + 1);
                        String bkupTable = filename.replace(".dbf", "");
                        log("Trendlog data (" + bkupTable.replace("temp", "") + ")");
                        extractZipEntryToFile(path + filename, zf, ze);

                        // Do a check to see if the table already exists, if so, provide the -a option to the restore
                        // script
                        boolean dataOnly = false;
                        WebDatabaseControl dbTrendControl = new WebDatabaseControl(Databases.TRENDLOGS);
                        try
                        {
                            Statement st = dbTrendControl.createStatement();
                            ResultSet rs = st.executeQuery("SELECT * FROM pg_tables WHERE tablename = " + Util.escapeAndQuote(bkupTable) + "");
                            if (rs.next())
                            {
                                dataOnly = true;
                            }
                            st.close();
                        }
                        catch (SQLException e1)
                        {
                           log(e1.getMessage());
                        }

                        Runtime rtime = Runtime.getRuntime();
                        Process child = rtime.exec("/bin/bash");
                        BufferedWriter outCommand = new BufferedWriter(new OutputStreamWriter(child.getOutputStream()));
                        String cmd = "sudo -u optergy /usr/local/Optergy/bin/scripts/restoreDataTable.sh " + Databases.TRENDLOGS.getDatabaseName() + " "
                                + filename + " " + bkupTable + " " + (dataOnly ? "-a" : "");
                        outCommand.write(cmd);
                        outCommand.flush();
                        outCommand.close();



                        BufferedReader br = new BufferedReader(new InputStreamReader(child.getInputStream()));
                        String str;
                        while ((str = br.readLine()) != null)
                        {
                           log( str);
                        }
                        br = new BufferedReader(new InputStreamReader(child.getErrorStream()));
                        while ((str = br.readLine()) != null)
                        {
                           log( str);
                        }
                        try
                        {
                            child.waitFor();
                        }
                        catch (InterruptedException e1)
                        {
                        }

                        if (child.exitValue() != 0)
                        {
                           log( "Error restoring Trendlogs database table "+ bkupTable +", error code: " + child.exitValue());
                            // drop existing table, keep a copy, restore table and then copy old data

                            if (!errorTrendlogTables.contains(bkupTable.replace("temp", "")))
                                errorTrendlogTables.add(bkupTable.replace("temp", ""));
                        }

                        String tableToVacuum = bkupTable;
                        // If filename (tablename) ends with temp, copy the data into tablename[-temp] and then drop the
                        // table
                        if (bkupTable.endsWith("temp"))
                        {
                            String restoreToTable = bkupTable.replace("temp", "");
                            tableToVacuum = restoreToTable;
                           log( "Restore to this table: " + restoreToTable);
                            // If dataOnly == false, the original table does not exist (most likely restoring to a fresh
                            // system, in
                            // which case, alter table instead of copying data
                            // SEE IF restoreToTable exists: if it does, copy the data from the temp table to
                            // restoreToTable,
                            // if not, do ALTER TABLE
                            try
                            {
                                Statement st = dbTrendControl.createStatement();
                                ResultSet rs = st.executeQuery("SELECT * FROM pg_tables WHERE tablename = " + Util.escapeAndQuote(restoreToTable) + "");
                                if (rs.next())
                                {
                                    // This inner select will make sure we are not insert records that already exist
                                    String innerSelect = "SELECT " + bkupTable + ".* FROM " + bkupTable + " LEFT JOIN " + restoreToTable + " ON " + bkupTable
                                            + ".id = " +
                                            restoreToTable + ".id WHERE " + restoreToTable + ".id IS NULL";
                                    st.executeUpdate("INSERT INTO " + restoreToTable + " " + innerSelect);
                                    st.executeUpdate("DROP TABLE " + bkupTable);
                                }
                                else
                                {
                                    st.execute("ALTER TABLE \"" + bkupTable + "\" RENAME TO \"" + restoreToTable + "\"");
                                }
                            }
                            catch (SQLException e1)
                            {
                               log(e1.getMessage());
                                if (!errorTrendlogTables.contains(restoreToTable))
                                    errorTrendlogTables.add(restoreToTable);
                            }
                        }
                        try
                        {
                            Statement st = dbTrendControl.createStatement();
                            st.execute("VACUUM FULL \"" + tableToVacuum + "\"");
                        }
                        catch (SQLException e1)
                        {
                           log(e1.getMessage());
                        }
                        Runtime run = Runtime.getRuntime();
                        Process running = run.exec("/bin/bash");
                        BufferedWriter outofCommands = new BufferedWriter(new OutputStreamWriter(running.getOutputStream()));
                        String console = "sudo -u optergy /usr/local/Optergy/bin/scripts/sequenceaddition.sh " + bkupTable;
                        outofCommands.write(console);
                        outofCommands.flush();
                        outofCommands.close();
                        BufferedReader brr = new BufferedReader(new InputStreamReader(running.getInputStream()));
                        String strr;
                        while ((strr = brr.readLine()) != null)
                        {
                           log( strr);
                        }
                        br = new BufferedReader(new InputStreamReader(running.getErrorStream()));
                        while ((strr = brr.readLine()) != null)
                        {
                           log( strr);
                        }
                        try
                        {
                            running.waitFor();
                        }
                        catch (InterruptedException e1)
                        {
                        }





                        dbTrendControl.close();

                        new File(path + filename).delete();
                    }
                    else if (ze.getName().startsWith("Energylogs/"))
                    {
                        log("Energylog data");
                        String path = "/usr/local/tomcat/temp/";
                        String filename = ze.getName().substring(ze.getName().lastIndexOf("/") + 1);
                        String bkupTable = filename.replace(".dbf", "");
                        extractZipEntryToFile(path + filename, zf, ze);
                        boolean failed = false;
                        // Make sure monit/metering is stopped
                        if (monitStopped == false)
                        {
                            try
                            {
                                String response = Util.executeUnixCommand("sudo invoke-rc.d monit stop", "System Restore: Stop Monit",null);
                                if (response == null)
                                {
                                    failed = true;
                                }
                            }
                            catch (Exception ex)
                            {
                               log( "Error stopping Monit in order to restore Energylog table");
                                this.msgText += filename;
                            }
                            finally
                            {
                                if (failed)
                                {
                                    errorEnergylogTables.add(bkupTable);
                                    continue;
                                }
                                monitStopped = true;
                            }
                        }

                        if (meteringStopped == false)
                        {
                            try
                            {
                                String response = Util.executeUnixCommand("sudo invoke-rc.d metering stop", "System Restore: Stop Metering",null);
                                if (response == null)
                                {
                                    failed = true;
                                }
                                // Delete the meter reading dat files so that metering picks up from whats in the database
                                String dataFilePath = "/usr/local/Optergy/data/metering/energyreadings/";
                                File folder = new File(dataFilePath);
                                FileUtils.cleanDirectory(folder);
                            }
                            catch (Exception ex)
                            {
                               log( "Error stopping metering in order to restore Energylog table");
                                this.msgText += filename;
                            }
                            finally
                            {
                                if (failed)
                                {
                                    errorEnergylogTables.add(bkupTable);
                                    continue;
                                }
                                meteringStopped = true;
                            }
                        }

                        // Do a check to see if the table already exists, if so, drop the table
                        WebDatabaseControl dbEnergylogControl = new WebDatabaseControl(Databases.ENERGYLOGS);
                        try
                        {
                            Statement st = dbEnergylogControl.createStatement();
                            ResultSet rs = st.executeQuery("SELECT * FROM pg_tables WHERE tablename = " + Util.escapeAndQuote(bkupTable) + "");
                            if (rs.next())
                            {
                                st.execute("DROP TABLE \"" + bkupTable + "\"");
                            }
                            rs = st.executeQuery("SELECT * FROM pg_tables WHERE tablename = " + Util.escapeAndQuote(bkupTable) + "_aggregate" + "");
                            if (rs.next())
                            {
                                st.execute("DROP TABLE \"" + bkupTable + "_aggregate" + "\"");
                            }
                            st.close();
                        }
                        catch (SQLException e1)
                        {
                           log(e1.getSQLState());
                        }

                        Runtime rtime = Runtime.getRuntime();
                        Process child = rtime.exec("/bin/bash");
                        BufferedWriter outCommand = new BufferedWriter(new OutputStreamWriter(child.getOutputStream()));
                        String cmd = "sudo -u optergy /usr/local/Optergy/bin/scripts/restoreDataTable.sh " + Databases.ENERGYLOGS.getDatabaseName() + " "
                                + filename + " " + bkupTable;
                        outCommand.write(cmd);
                        outCommand.flush();
                        outCommand.close();

                        BufferedReader br = new BufferedReader(new InputStreamReader(child.getInputStream()));
                        String str;
                        while ((str = br.readLine()) != null)
                        {
                            log(str);
                        }
                        br = new BufferedReader(new InputStreamReader(child.getErrorStream()));
                        while ((str = br.readLine()) != null)
                        {
                            log(str);
                        }
                        try
                        {
                            child.waitFor();
                        }
                        catch (InterruptedException e1)
                        {
                        }

                        if (child.exitValue() != 0)
                        {
                           log( "Error restoring EnergyLogs databases, error code: " + child.exitValue());
                            errorEnergylogTables.add(bkupTable);
                        }

                        try
                        {
                            Statement st = dbEnergylogControl.createStatement();
                            st.execute("VACUUM FULL \"" + bkupTable + "\"");
                        }
                        catch (SQLException e1)
                        {
                           log(e1.getMessage());
                        }

                        dbEnergylogControl.close();
                        new File(path + filename).delete();
                    }
                    else
                    {
                        if (ze.getName().equals("description.txt") || ze.getName().equals("summary.txt"))
                            continue;
                       log( "Unknown file: :" + ze.getName());
                    }
                }

                if (!errorEnergylogTables.isEmpty())
                {
                    this.msgText += ", error restoring energylog ";
                    for (String table : errorEnergylogTables)
                        this.msgText += table + ", ";
                }
                if (!errorTrendlogTables.isEmpty())
                {
                    this.msgText += ", error restoring trendlog ";
                    for (String table : errorTrendlogTables)
                        this.msgText += table + ", ";
                }

                if (meteringStopped)
                {
                    // Attempt to start Metering, if failed report back to screen
                    try
                    {
                        String response = Util.executeUnixCommand("sudo invoke-rc.d metering start", "System Restore: Start Metering",null);
                        if (response == null)
                            throw new Exception();
                    }
                    catch (Exception exec)
                    {
                       log( "Error starting Metering");
                        this.msgText += ",  Error starting Optergy Software (metering), Portal must be rebooted";
                    }
                }
            }

            sanitiseTrendlogDescriptions();

            // Attempt to start Optergy, if failed report back to screen
            try
            {

                // Update the database revision number
                File db_revision_file = new File("/usr/local/Optergy/data/database-revision");
                try {
                    db_revision_file.createNewFile();
                    int count = 0;
                    List<String> lines = Files.readAllLines(java.nio.file.Paths.get("/usr/local/Optergy/data/database-revision"), Charset.forName("US-ASCII"));
                    if (lines.size()>0) {
                        count = Integer.parseInt(lines.get(0));
                    }
                    count++;
                    PrintWriter writer = new PrintWriter("/usr/local/Optergy/data/database-revision");
                    writer.println(count);
                    writer.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

                String response = Util.executeUnixCommand("sudo invoke-rc.d optergy start", "System Restore: Start Optergy Software",null);
                if (response == null)
                    throw new Exception();
            }
            catch (Exception exec)
            {
               log( "Error starting Optergy Software in order to restore Optergy Software databases");
                this.msgText += ",  Error starting Optergy Software, Portal must be rebooted";
            }



            //need to read the webserver.backup file and put the kiosk mode values back into the working file
            //then delete the backup
            try
            {
                String BACKUP = "/usr/local/Optergy/conf/webserver.backup";
                String INUSE = ("/usr/local/Optergy/conf/webserver.properties");
                FileReader fileReader = new FileReader(BACKUP);
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                String currentLine;
                String kioskStatus = null;
                ArrayList <String> replacementList = new ArrayList();
                while((currentLine = bufferedReader.readLine())!=null)
                {
                    if(currentLine.contains("enable_kiosk_mode"))
                    {
                        kioskStatus = currentLine;
                    }
                }
                //now need to read in the current file and replace the enable_kiosk_mode string
                fileReader = new FileReader(INUSE);
                bufferedReader = new BufferedReader(fileReader);
                boolean hasBeenAdded = false;
                while((currentLine = bufferedReader.readLine())!=null)
                {
                    if(currentLine.contains("enable_kiosk_mode"))
                    {
                        currentLine = kioskStatus;
                        hasBeenAdded = true;
                    }
                    replacementList.add(currentLine);
                }
                bufferedReader.close();
                //this will happen if there is no enable_kiosk_mode line in the new file.
                if(!hasBeenAdded)
                {
                    replacementList.add(kioskStatus);
                }
                //now delete the conf file and then write out this list to it.
                File oldConfFile = new File(INUSE);
                File backupFile = new File(BACKUP);
                if(oldConfFile.exists())
                {
                    oldConfFile.delete();
                }
                if(backupFile.exists())
                {
                    backupFile.delete();
                }

                PrintWriter writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(oldConfFile,true),"UTF-8"));
                for(String output : replacementList)
                {
                    writer.println(output);
                }
                writer.close();
            }
            catch(Exception e)
            {
               log(e.getMessage());
            }

        }
        catch (IOException e)
        {
           log(e.getMessage());
            this.msgText += ", IOError running database restore";
        }
        catch (InterruptedException e5)
        {
            // TODO Auto-generated catch block
            e5.printStackTrace();
        }
        catch (PlatformException e5)
        {
            // TODO Auto-generated catch block
            e5.printStackTrace();
        }
        catch (ErrorCodeException e5)
        {
            // TODO Auto-generated catch block
            e5.printStackTrace();
        }
        finally {
            if(this.f.exists())
                f.delete();
        }

        boolean restartNeeded = (this.msgText == null || this.msgText.length() < 1) && shouldValidateLicence;



        // Validate license file just unpacked from backup & remove license features if validation fails.
        if (shouldValidateLicence)
        {
            this.validateLicence();
        }

        // set servlet context attributes to signal the restore completion.
        log("Complete");

        if (restartNeeded)
        {
            this.restartTomcat();
        }
    }

*/
    /**
     * Sanitise ALL trendlog descriptions of special characterse,
     * Encompassing any newly improted trendlogs
     */
    private void sanitiseTrendlogDescriptions() {
        DatabaseControl optergyDB = new DatabaseControl(Databases.OPTERGY);

        List<Trendlog> trendlogObjects;
        try {
            trendlogObjects = optergyDB.getBeanDealer().get(Trendlog.class);
            for (Trendlog trendlog : trendlogObjects) {

                trendlog.setDescription(trendlog.getDescription().replaceAll("[^a-zA-Z0-9{}():\\,\\.\\-\\s+]+", ""));
                optergyDB.getBeanDealer().update(trendlog);
            }
        } catch (SQLException | UnsuitableClassException e) {
            e.printStackTrace();
        }
    }


    /**
     * Restarts Optergy portal
     */
    private void restartSystem() {
        try {
            // Wait for 2 seconds before log out, let SystemRestore.jsp read context variables.
            Thread.sleep(2100);
            Runtime rtime = Runtime.getRuntime();
            Process child = rtime.exec("/bin/bash");
            BufferedWriter outCommand = new BufferedWriter(new OutputStreamWriter(child.getOutputStream()));
            outCommand.write("sudo shutdown now -r  > /dev/null");
            outCommand.flush();
            outCommand.close();
        } catch (InterruptedException | IOException e) {
            new SocketLogger(LogFile.WEBSERVER, this.logLevel, LogUtils.getLoggingHost()).logException(e);
        }
    }

    /**
     * Restarts Tomcat server after a wait of 2.1 seconds.
     */
    private void restartTomcat() {
        try {
            // Wait for 2 seconds before log out, let SystemRestore.jsp read context variables.
            Thread.sleep(2100);
            Runtime rtime = Runtime.getRuntime();
            Process child = rtime.exec("/bin/bash");
            BufferedWriter outCommand = new BufferedWriter(new OutputStreamWriter(child.getOutputStream()));
            outCommand.write("sudo invoke-rc.d tomcat restart &> /dev/null");
            outCommand.flush();
            outCommand.close();
        } catch (InterruptedException | IOException e) {
            new SocketLogger(LogFile.WEBSERVER, this.logLevel, LogUtils.getLoggingHost()).logException(e);
        }
    }


    /**
     * Logs out the current user logged in and kills the session.
     */


    /**
     * Validates the license file and updates User Activity
     *
     * @return
     */
    private void validateLicence() {
        LicenceManager licenceManager = LicenceManager.licence;
        try {
            licenceManager.validate();

            log("Licence check after system restore completed successfully.");
        } catch (LicenceValidationException e) {
            this.clearLicenceTables(licenceManager);
            log(e.getMessage());

        } catch (LicenceInvalidException e) {
            this.clearLicenceTables(licenceManager);
            log(e.getMessage());
        }

    }

    /**
     * Clears our license table of any data.
     * While restoring the license tables may have been populated with data.
     *
     * @param licenceManager
     */
    private void clearLicenceTables(LicenceManager licenceManager) {
        DatabaseControl dbControl = new DatabaseControl(Databases.OPTERGY);
        try {
            licenceManager.clearDatabase(dbControl);
        } catch (SQLException | UnsuitableClassException excpetion) {
            log(excpetion.getMessage());
        }
    }

    /**
     * Extracts a file from a zip file to the specified path
     *
     * @param path The path to extract the file to
     * @param zf   The zip file
     * @param ze   The {@link ZipEntry} in the {@link ZipFile}
     * @throws IOException
     */
    private void extractFile(String path, ZipFile zf, ZipEntry ze) {
        String filename = ze.getName();
        File dir = new File(path + filename);

        if (ze.isDirectory()) {
            // When creating directories, we are relying on the fact that non-existant parent directories are created
            // first
            // such that mkdirs actually only creates 1 directory at a time
            File destParent = dir.getParentFile();
            destParent.mkdirs();

            if (dir.exists() == false) {
                log("Creating dir " + path + filename);
                new File(path + filename).mkdir();
                Util.setFilePermission(path + filename, "777");
            }
            return;
        }

        try {
            InputStream inStream = zf.getInputStream(ze);
            OutputStream outStream = new FileOutputStream(path + filename);
            byte[] buffer = new byte[1024];
            int nrBytesRead;
            while ((nrBytesRead = inStream.read(buffer)) > 0) {
                outStream.write(buffer, 0, nrBytesRead);
            }
            inStream.close();
            outStream.close();
            Util.setFilePermission(path + filename, "666");
        } catch (FileNotFoundException e) {
            log("Permission error restoring file - File not found: " + filename);
        } catch (IOException e) {
            log("IO error restoring file: " + filename);

        }
    }

    /**
     * Extracts the file to the specified filename, not the filename in the zip file
     *
     * @param file The filename to extract the file in the zip file to
     * @param zf   The {@link ZipFile}
     * @param ze   The {@link ZipEntry} in the {@link ZipFile}
     * @throws IOException
     */
    private void extractZipEntryToFile(String file, ZipFile zf, ZipEntry ze) {
        try {
            InputStream inStream = zf.getInputStream(ze);
            OutputStream outStream = new FileOutputStream(file);
            byte[] buffer = new byte[1024];
            int nrBytesRead;
            while ((nrBytesRead = inStream.read(buffer)) > 0) {
                outStream.write(buffer, 0, nrBytesRead);
            }
            inStream.close();
            outStream.close();
            Util.setFilePermission(file, "666");
        } catch (FileNotFoundException e) {
            log("Permission error restoring file - File not found: " + file);
        } catch (IOException e) {
            log("IO error restoring file: " + file);
        }
    }


}